//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "sketch/operators/Create.h"

#include "sketch/Resource.h"
#include "Create_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "sketch/json/jsonResource.h"

#include "smtk/resource/json/Helper.h"

#include "smtk/common/Paths.h"

#include <fstream>

using namespace smtk::model;

namespace sketch
{

Create::Result Create::operateInternal()
{
  // TODO: Create a resource, set its location (if one is provided) and add
  //       the resource to the result.
  auto result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);

  return result;
}

const char* Create::xmlDescription() const
{
  return Create_xml;
}

void Create::markModifiedResources(Result& result)
{
  auto resource = result->findResource("resource")->value();
  if (resource)
  {
    resource->setClean(true);
  }
}

smtk::resource::ResourcePtr create(
  const smtk::common::UUID& uid,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  // TODO: You *may* provide the resource manager with a free function
  //       to create resources of your new type. Implement this function
  //       by creating, configuring, and running an instance of the Create
  //       operation. Be sure to return the resource on success.
  return nullptr;
}

} // namespace sketch
