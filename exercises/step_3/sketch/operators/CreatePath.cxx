//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "sketch/operators/CreatePath.h"

#include "sketch/Path.h"
#include "sketch/Resource.h"
#include "CreatePath_xml.h"

#include "smtk/operation/MarkGeometry.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/attribute/VoidItem.h"

#include "sketch/json/jsonResource.h"

#include "smtk/resource/json/Helper.h"

#include "vtkCellArray.h"
#include "vtkPolyData.h"
#include "vtkPoints.h"

using namespace smtk::model;

namespace sketch
{

CreatePath::Result CreatePath::operateInternal()
{
  auto params = this->parameters();
  auto resource = params->associations()->valueAs<sketch::Resource>();
  CreatePath::Result result;
  if (!resource)
  {
    smtkErrorMacro(this->log(), "No resource provided.");
    result = this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  else
  {
    auto curveGroup = params->findGroup("curve");
    auto pointsItem = curveGroup->findAs<smtk::attribute::DoubleItem>(0, "points");
    auto closedItem = curveGroup->findAs<smtk::attribute::VoidItem>(0, "closed");
    std::shared_ptr<sketch::Path> path;
    if (pointsItem->numberOfValues() >= 6)
    {
      path = resource->create<sketch::Path>();
      path->setName("new path");

      vtkNew<vtkPolyData> geometry;
      vtkNew<vtkPoints> points;
      vtkNew<vtkCellArray> lineCell;
      geometry->SetPoints(points);
      auto np = static_cast<vtkIdType>(pointsItem->numberOfValues() / 3); // 3 coordinates per point.
      points->SetNumberOfPoints(np);
      // Copy point coordinates into VTK data and create the polyline.
      lineCell->Reset();
      lineCell->InsertNextCell(closedItem->isEnabled() ? np + 1 : np);
      for (vtkIdType ii = 0; ii < np; ++ii)
      {
        points->SetPoint(ii,
          pointsItem->value(3 * ii),
          pointsItem->value(3 * ii + 1),
          pointsItem->value(3 * ii + 2));
        lineCell->InsertCellPoint(ii);
      }
      if (closedItem->isEnabled())
      {
        lineCell->InsertCellPoint(0); // Closed loops repeat the first point.
      }
      geometry->SetLines(lineCell);
      path->geometry()->ShallowCopy(geometry);

      result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
      result->findComponent("created")->appendValue(path);
      smtk::operation::MarkGeometry().markModified(path); // Irrelevant until exercise 3.2.
    }
    else
    {
      smtkErrorMacro(this->log(), "Invalid number of coordinates "
        << pointsItem->numberOfValues() << " provided.");
      result = this->createResult(smtk::operation::Operation::Outcome::FAILED);
    }
  }
  return result;
}

const char* CreatePath::xmlDescription() const
{
  return CreatePath_xml;
}

} // namespace sketch
