//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "sketch/operators/CreateGroup.h"

#include "sketch/Group.h"
#include "CreateGroup_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "sketch/json/jsonResource.h"

#include "smtk/resource/json/Helper.h"

using namespace smtk::model;

namespace sketch
{

CreateGroup::Result CreateGroup::operateInternal()
{
  auto params = this->parameters();
  auto assoc = params->associations();
  std::set<sketch::Path::Ptr> members;
  sketch::Resource* resource = nullptr;
  for (std::size_t ii = 0; ii < assoc->numberOfValues(); ++ii)
  {
    if (assoc->isSet(ii))
    {
      auto item = std::dynamic_pointer_cast<sketch::Path>(assoc->value(ii));
      if (item)
      {
        if (!resource)
        {
          resource = dynamic_cast<sketch::Resource*>(item->parentResource());
        }
        members.insert(item);
      }
    }
  }
  CreateGroup::Result result;
  if (!resource || members.empty())
  {
    smtkErrorMacro(this->log(), "No components to group or no components had parent resource.");
    result = this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  else
  {
    auto group = resource->create<sketch::Group>();
    for (const auto& member : members)
    {
      group->outgoing<GroupsToPaths>().connect(member);
    }
    std::ostringstream name;
    group->setName("new group");
    result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
    result->findComponent("created")->appendValue(group);
    // Groups have a footprint, but not geometry themselves.
    // smtk::operation::MarkGeometry().markModified(group);
  }
  return result;
}

const char* CreateGroup::xmlDescription() const
{
  return CreateGroup_xml;
}

} // namespace sketch
