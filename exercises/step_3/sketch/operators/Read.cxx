//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "sketch/operators/Read.h"

#include "sketch/Resource.h"
#include "Read_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "sketch/json/jsonResource.h"

#include "smtk/resource/json/Helper.h"

#include "smtk/common/Paths.h"

#include <fstream>

namespace sketch
{

bool Read::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  auto filename = this->parameters()->findFile("filename")->value();
  std::ifstream file(filename);
  if (!file.good())
  {
    return false;
  }
  return true;
}

Read::Result Read::operateInternal()
{
  auto filename = this->parameters()->findFile("filename")->value();
  std::ifstream file(filename);
  if (!file.good())
  {
    smtkErrorMacro(this->log(), "Could not open file \"" << filename << "\" for reading.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  nlohmann::json jj;
  try
  {
    jj = nlohmann::json::parse(file);
  }
  catch (...)
  {
    smtkErrorMacro(
      this->log(), "File \"" << filename << "\" is not in JSON format.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  auto resource = sketch::Resource::create();
  // TODO: Deserialize the contents of the file into this newly created
  //       resource. Don't forget to push the resource onto the stack of
  //       json helper resources!

  auto result = this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED);
  // TODO: Add the resource to the result's "resource" item.
  return result;
}

const char* Read::xmlDescription() const
{
  return Read_xml;
}

void Read::markModifiedResources(Read::Result& result)
{
  auto resourceItem = result->findResource("resource");
  for (std::size_t ii = 0; ii < resourceItem->numberOfValues(); ++ii)
  {
    if (!resourceItem->isSet(ii))
    {
      continue;
    }
    auto resource = std::dynamic_pointer_cast<sketch::Resource>(resourceItem->value(ii));

    // Set the resource as unmodified from its persistent (i.e. on-disk) state
    if (resource)
    {
      resource->setClean(true);
    }
  }
}

smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  // TODO: You must provide the resource manager with a free function
  //       that can read resources of your new type. Implement this
  //       function by creating, configuring, and running an instance
  //       of this Read operation. Then fetch the resource from the
  //       result and return it.
  return nullptr;
}

} // namespace sketch
