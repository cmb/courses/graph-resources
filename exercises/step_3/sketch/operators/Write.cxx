//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "sketch/operators/Write.h"

#include "sketch/Resource.h"
#include "Write_xml.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "sketch/json/jsonResource.h"

#include "smtk/common/Paths.h"

using namespace smtk::model;

namespace sketch
{

bool Write::ableToOperate()
{
  if (!this->Superclass::ableToOperate())
  {
    return false;
  }

  auto associations = this->parameters()->associations();
  if (associations->numberOfValues() != 1 || !associations->isSet())
  {
    smtkWarningMacro(this->log(), "A resource must be provided.");
    return false;
  }

  auto resource = associations->valueAs<sketch::Resource>();
  if (!resource || resource->location().empty())
  {
    smtkWarningMacro(this->log(), "Resource must have a valid location and be a sketch resource.");
    return false;
  }

  // TODO: We could also check that the location is writable.

  return true;
}

Write::Result Write::operateInternal()
{
  auto resourceItem = this->parameters()->associations();
  sketch::Resource::Ptr rsrc = std::dynamic_pointer_cast<sketch::Resource>(resourceItem->value());

  // TODO: Serialize resource into a set of JSON records and write it to the
  //       resource's location().
  bool ok = false;

  return ok ? this->createResult(smtk::operation::Operation::Outcome::SUCCEEDED)
            : this->createResult(smtk::operation::Operation::Outcome::FAILED);
}

const char* Write::xmlDescription() const
{
  return Write_xml;
}

void Write::markModifiedResources(Write::Result& /*unused*/)
{
  auto resourceItem = this->parameters()->associations();
  for (auto rit = resourceItem->begin(); rit != resourceItem->end(); ++rit)
  {
    auto resource = std::dynamic_pointer_cast<smtk::resource::Resource>(*rit);

    // Set the resource as unmodified from its persistent (i.e. on-disk) state
    resource->setClean(true);
  }
}

bool write(
  const smtk::resource::ResourcePtr& resource,
  const std::shared_ptr<smtk::common::Managers>& managers)
{
  // TODO: You must provide the resource manager with a free function
  //       to write resources of your new type. Implement this function
  //       by creating, configuring, and running an instance of the Write
  //       operation. Be sure to return true on success.
  return false;
}

} // namespace sketch
