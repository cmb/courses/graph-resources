//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "Geometry.h"

#include "Cusp.h"
#include "Path.h"
#include "Resource.h"

#include "smtk/geometry/Generator.h"

#include "vtkCompositeDataSet.h"
#include "vtkDataSetSurfaceFilter.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkNew.h"
#include "vtkCubeSource.h"
#include "vtkSplineFilter.h"

namespace sketch
{

Geometry::Geometry(const std::shared_ptr<sketch::Resource>& parent)
  : m_parent(parent)
{
}

smtk::geometry::Resource::Ptr Geometry::resource() const
{
  return std::dynamic_pointer_cast<smtk::geometry::Resource>(m_parent.lock());
}

void Geometry::queryGeometry(const smtk::resource::PersistentObject::Ptr& obj, CacheEntry& entry)
  const
{
  // Access the markup component
  auto component = std::dynamic_pointer_cast<sketch::Node>(obj);
  if (!component)
  {
    entry.m_generation = Invalid;
    return;
  }

  entry.m_geometry = nullptr;

  // Handle paths
  if (const auto* path = dynamic_cast<const sketch::Path*>(component.get()))
  {
    // TODO: Pass the Path's geometry straight through to be rendered.
    //       This is the advantage of storing VTK data as a member variable of Path.
    std::cout << "TODO: Copy VTK data from the path to the geometry cache.\n";

    // TODO: As a bonus, if the path is attached to a Style node, color the path using the style.
  }
  else if (const auto* cusp = dynamic_cast<const sketch::Cusp*>(component.get()))
  {
    // TODO: Since Cusp objects do not store their coordinates in VTK format,
    //       we must create a VTK data object that holds a vertex cell.

    // The Style class does not currently hold a vertex color or we
    // would apply it here.
  }

  // Now either increment the generation number of the cache entry
  // or invalidate it, depending on whether we were able to provide
  // renderable geometry.
  // This way consumers can compare generation numbers to discover
  // when the cache entry has changed.
  if (entry.m_geometry)
  {
    ++entry.m_generation;
  }
  else
  {
    entry.m_generation = Invalid;
    return;
  }
}

int Geometry::dimension(const smtk::resource::PersistentObject::Ptr& obj) const
{
  if (const auto* path = dynamic_cast<const sketch::Path*>(obj.get()))
  {
    return 1;
  }
  else if (const auto* cusp = dynamic_cast<const sketch::Cusp*>(obj.get()))
  {
    return 0;
  }
  return 0;
}

Geometry::Purpose Geometry::purpose(const smtk::resource::PersistentObject::Ptr&) const
{
  // We do not use glyph mapping for any node types.
  return Geometry::Surface;
}

void Geometry::update() const
{
  // Do nothing. Operations in smtk markup set content as needed.
}

void Geometry::geometricBounds(const DataType& geom, BoundingBox& bbox) const
{
  auto* pset = vtkPointSet::SafeDownCast(geom);
  if (pset)
  {
    pset->GetBounds(bbox.data());
    return;
  }
  auto* comp = vtkCompositeDataSet::SafeDownCast(geom);
  if (comp)
  {
    comp->GetBounds(bbox.data());
    return;
  }

  // Invalid bounding box:
  bbox[0] = bbox[2] = bbox[4] = 0.0;
  bbox[1] = bbox[3] = bbox[5] = -1.0;
}
} // namespace sketch
