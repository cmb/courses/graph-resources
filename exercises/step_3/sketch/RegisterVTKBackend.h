// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_RegisterVTKBackend_h
#define sketch_RegisterVTKBackend_h
#ifndef __VTK_WRAP__

#include "Resource.h"
#include "Geometry.h"

#include "smtk/extension/vtk/geometry/Backend.h"
#include "smtk/extension/vtk/geometry/Registrar.h"
#include "smtk/geometry/Generator.h"
#include "smtk/geometry/Manager.h"

namespace sketch
{

class SKETCHSTEP3_EXPORT RegisterVTKBackend
  : public smtk::geometry::Supplier<RegisterVTKBackend>
{
public:
  bool valid(const smtk::geometry::Specification& in) const override
  {
    smtk::extension::vtk::geometry::Backend backend;
    return std::get<1>(in).index() == backend.index();
  }

  GeometryPtr operator()(const smtk::geometry::Specification& in) override
  {
    auto rsrc = std::dynamic_pointer_cast<sketch::Resource>(std::get<0>(in));
    if (rsrc)
    {
      auto* provider = new Geometry(rsrc);
      return GeometryPtr(provider);
    }
    throw std::invalid_argument("Not a sketch resource.");
    return nullptr;
  }
};
} // namespace sketch

#endif // __VTK_WRAP__
#endif // sketch_RegisterVTKBackend_h
