//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "sketch/Registrar.h"

#include "sketch/Geometry.h"
#include "sketch/RegisterVTKBackend.h"

#include "sketch/operators/Create.h"
#include "sketch/operators/CreatePath.h"
#include "sketch/operators/CreateGroup.h"
#include "sketch/operators/Read.h"
#include "sketch/operators/SetName.h"
#include "sketch/operators/Write.h"

#include "sketch/Resource.h"

#include "smtk/operation/groups/CreatorGroup.h"
#include "smtk/operation/groups/NamingGroup.h"
#include "smtk/operation/groups/ReaderGroup.h"
#include "smtk/operation/groups/WriterGroup.h"

namespace sketch
{

namespace
{
using OperationList = std::tuple<
  Create,
  CreatePath,
  CreateGroup,
  Read,
  SetName,
  Write
>;
} // anonymous namespace

void Registrar::registerTo(const smtk::resource::Manager::Ptr& resourceManager)
{
  // Providing a write method to the resource manager is what allows
  // modelbuilder's pqSMTKSaveResourceBehavior to determine how to write
  // the resource when users click "Save Resource" or ⌘ S/⌃S.
  resourceManager->registerResource<sketch::Resource>(read, write, create);
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  // Register operations
  operationManager->registerOperations<OperationList>();

  // Add operations to groups
  smtk::operation::CreatorGroup(operationManager)
    .registerOperation<
      sketch::Resource,
      sketch::Create>();

  smtk::operation::NamingGroup(operationManager)
    .registerOperation<sketch::Resource, sketch::SetName>();

  smtk::operation::ReaderGroup(operationManager)
    .registerOperation<sketch::Resource, sketch::Read>();

  smtk::operation::WriterGroup(operationManager)
    .registerOperation<sketch::Resource, sketch::Write>();
}

void Registrar::registerTo(const smtk::geometry::Manager::Ptr& geometryManager)
{
  // TODO: Register the VTK backend here using RegisterVTKBackend.
  std::cout << "TODO: Register markup to geometry manager\n";
}

void Registrar::unregisterFrom(const smtk::resource::Manager::Ptr& resourceManager)
{
  resourceManager->unregisterResource<sketch::Resource>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  smtk::operation::CreatorGroup(operationManager)
    .unregisterOperation<sketch::Create>();

  smtk::operation::NamingGroup(operationManager)
    .unregisterOperation<sketch::SetName>();

  smtk::operation::ReaderGroup(operationManager)
    .unregisterOperation<sketch::Read>();

  smtk::operation::WriterGroup(operationManager)
    .unregisterOperation<sketch::Write>();

  operationManager->unregisterOperations<OperationList>();
}

void Registrar::unregisterFrom(const smtk::geometry::Manager::Ptr& geometryManager)
{
  geometryManager->unregisterBackend<smtk::extension::vtk::geometry::Backend>();
}

} // namespace sketch
