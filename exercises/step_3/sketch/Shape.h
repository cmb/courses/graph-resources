// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step3_Shape_h
#define sketch_step3_Shape_h

#include "Node.h"

namespace sketch
{

class Style;

class SKETCHSTEP3_EXPORT Shape : public Node
{
public:
  smtkTypeMacro(sketch::Shape);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Shape(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  /// This is a convenience method for fetching the most applicable rendering style.
  const Style* style() const;
};

} // namespace sketch

#endif // sketch_step3_Shape_h
