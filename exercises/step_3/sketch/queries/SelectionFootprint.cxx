#include "sketch/queries/SelectionFootprint.h"

#include "sketch/Traits.h"

namespace sketch
{
namespace queries
{

bool SelectionFootprint::operator()(
  smtk::resource::PersistentObject& selectedObject,
  std::unordered_set<smtk::resource::PersistentObject*>& footprint,
  const smtk::geometry::Backend& backend) const
{
  bool haveFootprint = false;
  // TODO: Check the type of selectedObject and add to the footprint
  //       as needed:
  //       + Paths and cusps are their own footprint.
  //       + The footprint of a group is all its members.
  //       + The footprint of a style is all the shapes marked with that style.
  return haveFootprint;
}

} // namespace queries
} // namespace sketch
