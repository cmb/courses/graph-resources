Step 3 Exercises
----------------

The first exercise in this step will produce an SMTK resource file
named ``example.smtk`` when you complete the code and run the
executable. This resource file is useful later.

Unlike previous steps, exercises 2 and 3 in this step do not have
tests for you to run. Instead, when you are done with them, you
will have a modelbuilder plugin (named
``lib/smtk-22.07.00/smtkSketchStep3Plugin/smtkSketchStep3Plugin.so``)
that you can load and exercise by reading and writing "sketch"
resources.

Once you load the plugin (using Tools→Manage Plugins…), be sure to
mark the plugin for "Autoload". Then close modelbuilder and restart.
Autoloading the plugin causes it to be initialized earlier and
avoids additional configuration work at startup.

1. **Read, Write, and Create Operations**.
   Compare the source code in this directory with that from step 2.
   You'll see that there are read, write, and create operations
   declared but with empty implementations.
   Run the ``Exercise3_1`` exercise by passing ``Exercise3_1`` to
   the ``bin/exercises_step_3`` executable and observe
   that it exits early.
   Replace the print statements in the operators with
   implementations that write, read, and create resources.
   Rerun ``Exercise3_1`` to verify that they work.

   There are other operations provided besides the 3 you must implement;
   they are there so that our example resource will be testable from
   within modelbuilder.
   Feel free to look at how they are implemented.
   We will cover them in more detail on the last day of the course.

2. **Renderable Geometry**.
   In order to render paths and cusp points, we'll need to provide
   VTK data that corresponds to each.
   We've added point coordinates to the Cusp class, but we haven't
   done anything for the Path class.
   While we could follow the same approach as for cusps, we'll hold
   VTK data directly instead. This complicates writing JSON data
   but simplifies rendering. It also provides you with examples of
   the two basic approaches for handling renderable geometry.

   This involves several changes:
   a. Adding a member variable holding VTK data to Path.
   b. Adding a geometry backend that supports our resource and components.
   c. Registering the backend.
   d. Creating a plugin for modelbuilder.

   Find the TODO comments in ``Geometry.cxx`` and ``Registrar.cxx``;
   then, complete the classes as directed.

3. **Selection Footprint***.
   Now that we can render paths and cusps in modelbuilder, it is clear
   that interaction is a little lacking; clicking on a group in the
   resource panel does not highlight its paths.
   This is the job of a selection footprint query.
   We learned about queries previously; now we will implement one.
   If you look at the differences in the Resource class between steps 2 and 3:

   .. code-block:: bash

      meld step_2/sketch/Resource.cxx step_3/sketch/Resource.cxx

   you'll see that we now register a ``SelectionFootprint`` query.
   Your job is to provide an implementation in ``sketch/queries/SelectionFootprint.cxx``.
