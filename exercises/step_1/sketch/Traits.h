// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step1_Traits_h
#define sketch_step1_Traits_h

#include "sketch/Exports.h"

#include "smtk/graph/Component.h"

#include <type_traits> // for std::true_type
#include <tuple>

namespace sketch
{

// Forward-declare node types

class Cusp;
class Group;
class Path;
class Style;

// Declare arc traits

/// Paths can be grouped together.
struct SKETCHSTEP1_EXPORT GroupsToPaths
{
  using FromType = Group;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 group.
};

/// Paths can be smooth, closed curves (no cusps) or have cusps (at ends or along interior).
struct SKETCHSTEP1_EXPORT PathsToCusps
{
  using FromType = Path;
  using ToType = Cusp;
  using Directed = std::true_type;
};

/// Paths can have a style.
struct SKETCHSTEP1_EXPORT StylesToPaths
{
  using FromType = Style;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 style.
};

/// Node and arc types for hand drawings.
struct SKETCHSTEP1_EXPORT Traits
{
  using NodeTypes = std::tuple<Cusp, Group, Path, Style>;
  using ArcTypes = std::tuple<GroupsToPaths, PathsToCusps, StylesToPaths>;
};

class SKETCHSTEP1_EXPORT Node : public smtk::graph::Component
{
public:
  smtkTypeMacro(sketch::Node);
  smtkSuperclassMacro(smtk::graph::Component);

  template<typename... Args>
  Node(Args&&... args)
    : smtk::graph::Component(std::forward<Args>(args)...)
  {
  }

  std::string name() const override
  {
    if (this->properties().contains<std::string>("name"))
    {
      return this->properties().at<std::string>("name");
    }
    return this->Superclass::name();
  }
  void setName(const std::string& nodeName)
  {
    this->properties().get<std::string>()["name"] = nodeName;
  }
};

/// A node representing a C-0 point on a path (i.e., a corner or endpoint).
class SKETCHSTEP1_EXPORT Cusp : public Node
{
public:
  smtkTypeMacro(sketch::Cusp);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Cusp(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a collection of paths.
class SKETCHSTEP1_EXPORT Group : public Node
{
public:
  smtkTypeMacro(sketch::Group);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Group(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a single stroke of a pen.
class SKETCHSTEP1_EXPORT Path : public Node
{
public:
  smtkTypeMacro(sketch::Path);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Path(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing the drawing style for a path (e.g. color, thickness, ...)
class SKETCHSTEP1_EXPORT Style : public Node
{
public:
  smtkTypeMacro(sketch::Style);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Style(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  /// Set/get a color to use when rendering lines.
  ///
  /// Note that this is stored as a member variable - not a property - while
  /// line thickness is stored as a property. This was done for illustrative
  /// purposes.
  void setLineColor(const std::array<double, 4>& color)
  {
    m_lineColor = color;
  }
  const std::array<double, 4>& lineColor() const { return m_lineColor; }

  /// Set/get a thickness to use for lines when rendering.
  void setLineThickness(double thickness)
  {
    this->properties().get<double>()["line_thickness"] = thickness;
  }
  double lineThickness() const
  {
    if (this->properties().contains<double>("line_thickness"))
    {
      return this->properties().at<double>("line_thickness");
    }
    return -1.0;
  }

protected:
  std::array<double, 4> m_lineColor{0.9, 0.9, 0.9, 1.0};
};

} // namespace sketch

#endif // sketch_step1_Traits_h
