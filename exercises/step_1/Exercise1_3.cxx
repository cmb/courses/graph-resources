#include "sketch/Resource.h"

#include <memory>

int Exercise1_3(int, char**)
{
  using GroupsToPaths = sketch::GroupsToPaths;
  using StylesToPaths = sketch::StylesToPaths;

  auto resource = sketch::Resource::create();
  resource->setName("example");

  // Create some nodes.
  auto path1 = resource->create<sketch::Path>();
  auto path2 = resource->create<sketch::Path>();
  auto group = resource->create<sketch::Group>();
  auto plain = resource->create<sketch::Style>();
  auto fancy = resource->create<sketch::Style>();
  path1->setName("path 1");
  path2->setName("path 2");
  group->setName("group");
  plain->setName("plain style");
  fancy->setLineColor({0.6, 0.2, 0.2, 1.0});
  fancy->setLineThickness(2.0);
  fancy->setName("fancy style");

  // Add some cusp nodes
  // TODO: PUT YOUR SOLUTION TO EXERCISES 1 AND 4 HERE.

  // Set point coordinates on the cusps:
  // TODO: Uncomment the lines below to set point coordinates for EXERCISE 2.
  // cusp1->setCoordinates({1.0, 0.0});
  // cusp2->setCoordinates({2.0, 1.0});

  // Connect the nodes into a graph.
  group->outgoing<GroupsToPaths>().connect(path1);
  group->outgoing<GroupsToPaths>().connect(path2);
  plain->outgoing<StylesToPaths>().connect(path1);
  fancy->outgoing<StylesToPaths>().connect(path2);
  // TODO: Hint for EXERCISE 3. Uncomment the lines below to connect
  //       the cusps from earlier exercises to style nodes.
  // cusp1->incoming<???>().connect(plain);
  // cusp2->incoming<???>().connect(fancy);

  // Dump the resulting graph.
  resource->dump("", "text/vnd.graphviz");
  return 0;
}
