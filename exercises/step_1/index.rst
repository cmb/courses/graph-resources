Step 1 Exercises
----------------

0. Run the ``StartingPoint1`` exercise by passing ``StartingPoint1`` to
   the executable created for all the exercises.
   Note that you can inspect the result like so:

   .. code-block:: bash

      ./bin/exercises_step_1 StartingPoint1 > graph.dot
      dot -Tpng graph.dot > graph.png
      eog graph.png


   The first line in the script above saves the test output to a file.
   The second line runs dot (a tool that comes with graphviz) to generate
   a layout of the graph and save an image of the result.
   The third line above displays the image.

1. **Adding nodes to a graph resource**.
   Modify the ``Exercise1_1`` source code to create two cusps as endpoints
   for ``path1``. Build and run it, then inspect the output using ``dot``.

2. **Adding member data to node types**.
   Modify the ``Cusp`` class to hold an (x,y)-tuple as coordinates.
   Try using properties (as opposed to a protected member variable).
   Note that the ``Exercise1_2`` test has some commented-out lines
   that will assign coordinates to the cusps from exercise 1 above.

3. **Exploring arc node-type constraints**.
   Provide a way for ``Style`` nodes to connect to ``Cusp`` nodes as
   well as ``Path`` nodes (but not to ``Group`` nodes).
   *Hint: there are several ways to do this. What are the
   advantages and disadvantages of each?*

4. **Extra credit: Getting used to verbose errors**.
   Knowing what errors from common mistakes look like will help you debug
   problems with more complex resources in the future, so let's introduce
   a "common mistake" and see what we get.
   Try connecting the plain or fancy style objects to the cusps you
   created in exercise 1 above. Do you get build errors? runtime errors?
