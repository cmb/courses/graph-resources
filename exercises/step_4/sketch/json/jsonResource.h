// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step4_json_jsonResource_h
#define sketch_step4_json_jsonResource_h

#include "sketch/Resource.h"

#include "nlohmann/json.hpp"

namespace sketch
{

SKETCHSTEP4_EXPORT void to_json(nlohmann::json& j, const sketch::Resource::Ptr& resource);

SKETCHSTEP4_EXPORT void from_json(const nlohmann::json& j, sketch::Resource::Ptr& resource);

} // namespace sketch

#endif // sketch_step4_json_jsonResource_h
