// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step4_Group_h
#define sketch_step4_Group_h

#include "Node.h"

namespace sketch
{

/// A node representing a collection of paths.
class SKETCHSTEP4_EXPORT Group : public Node
{
public:
  smtkTypeMacro(sketch::Group);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Group(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

} // namespace sketch

#endif // sketch_step4_Group_h
