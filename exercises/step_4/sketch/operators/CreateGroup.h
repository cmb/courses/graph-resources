//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_CreateGroup_h
#define sketch_CreateGroup_h

#include "sketch/Resource.h"

#include "smtk/operation/XMLOperation.h"

namespace sketch
{

/**\brief CreateGroup a sketch group node.
  */
class SKETCHSTEP4_EXPORT CreateGroup : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(sketch::CreateGroup);
  smtkCreateMacro(CreateGroup);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace sketch

#endif // sketch_CreateGroup_h
