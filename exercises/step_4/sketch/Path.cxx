// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "Path.h"

#include "vtkCellArray.h"
#include "vtkPoints.h"

namespace sketch
{

void Path::setPoints(const std::vector<double>& coordinates, bool closed)
{
  vtkNew<vtkPoints> points;
  vtkNew<vtkCellArray> lineCell;
  auto numPoints = static_cast<vtkIdType>(coordinates.size() / 3);
  points->SetNumberOfPoints(numPoints);
  lineCell->InsertNextCell(closed ? numPoints + 1 : numPoints);
  for (vtkIdType ii = 0; ii < numPoints; ++ii)
  {
    points->SetPoint(ii, &coordinates[3 * ii]);
    lineCell->InsertCellPoint(ii);
  }
  if (closed)
  {
    lineCell->InsertCellPoint(0);
  }
  m_geometry->SetPoints(points);
  m_geometry->SetLines(lineCell);
}

std::vector<double> Path::points() const
{
  std::vector<double> pts;
  auto* vpts = m_geometry->GetPoints();
  vtkIdType numPoints = vpts ? vpts->GetNumberOfPoints() : 0;
  if (numPoints > 0)
  {
    pts.resize(3 * numPoints);
    for (vtkIdType ii = 0; ii < numPoints; ++ii)
    {
      vpts->GetPoint(ii, &pts[3 * ii]);
    }
  }
  return pts;
}

bool Path::closed() const
{
  auto* lines = m_geometry->GetLines();
  if (!lines)
  {
    return false;
  }
  const vtkIdType* conn;
  vtkIdType numConn;
  lines->GetCellAtId(0, numConn, conn);
  return numConn < 1 ? false : conn[0] == conn[numConn - 1];
}

} // namespace sketch
