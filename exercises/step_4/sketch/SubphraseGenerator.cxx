// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "sketch/SubphraseGenerator.h"

#include "sketch/NodeGroupPhraseContent.h"
#include "sketch/Traits.h"

#include "smtk/view/ComponentPhraseContent.h"
#include "smtk/view/PhraseModel.h"
#include "smtk/view/ResourcePhraseContent.h"

#include "smtk/common/StringUtil.h"

#include <iostream>

using smtk::view::ComponentPhraseContent;
using smtk::view::ResourcePhraseContent;

static int phraseMutability =
  static_cast<int>(smtk::view::PhraseContent::ContentType::TITLE) |
  static_cast<int>(smtk::view::PhraseContent::ContentType::COLOR);

namespace sketch
{

SubphraseGenerator::SubphraseGenerator()
{
}

smtk::view::DescriptivePhrases SubphraseGenerator::subphrases(smtk::view::DescriptivePhrase::Ptr parent)
{
  smtk::view::DescriptivePhrases result;
  if (!parent)
  {
    return result;
  }
  auto* content = parent->content().get();
  const int mutability = static_cast<int>(smtk::view::PhraseContent::TITLE | smtk::view::PhraseContent::VISIBILITY);
  if (auto* componentContent = dynamic_cast<ComponentPhraseContent*>(content))
  {
    if (auto* group = dynamic_cast<sketch::Group*>(componentContent->relatedRawComponent()))
    {
      // Groups have members underneath them
      group->outgoing<GroupsToPaths>().visit([&result, &parent](const sketch::Path* constPath)
        {
          auto* path = const_cast<sketch::Path*>(constPath);
          result.push_back(
            ComponentPhraseContent::createPhrase(path->shared_from_this(), mutability, parent));
        }
      );
    }
    else if (auto* style = dynamic_cast<sketch::Style*>(componentContent->relatedRawComponent()))
    {
      // TODO: Finish implementing these cases.
      std::cout << "TODO: Add subphrases of style nodes.\n";
    }
    else if (auto* path = dynamic_cast<sketch::Path*>(componentContent->relatedRawComponent()))
    {
      // TODO: Finish implementing these cases.
      std::cout << "TODO: Add subphrases of path nodes.\n";
    }
    else if (auto* cusp = dynamic_cast<sketch::Cusp*>(componentContent->relatedRawComponent()))
    {
      // Cusps have no children.
    }
  }
  else if (auto* resourceContent = dynamic_cast<ResourcePhraseContent*>(content))
  {
    // Top-level resources have node groups underneath them
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Group")); // , "group"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Path")); // , "path"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Cusp")); // , "cusp"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Style")); // , "style"));
  }
  else if (auto* nodeGroupContent = dynamic_cast<sketch::NodeGroupPhraseContent*>(content))
  {
    // TODO: Finish implementing these cases.
    std::cout << "TODO: Add subphrases of group nodes.\n";
  }
  return result;
}

bool SubphraseGenerator::hasChildren(const smtk::view::DescriptivePhrase& parent) const
{
  bool result = false;
  // TODO: Implement this method. It is very similar to the subphrases() method
  //       but only returns true/false instead of a vector of phrases.
  std::cout << "TODO: Implement SubphraseGenerator::hasChildren().\n";
  return result;
}

smtk::resource::PersistentObjectSet SubphraseGenerator::parentObjects(
  const smtk::resource::PersistentObjectPtr& obj) const
{
  // TODO: Implement this method. It is very similar to the subphrases() method
  //       but returns a set of objects instead of a vector of phrases.
  std::cout << "TODO: Implement SubphraseGenerator::parentObjects().\n";
  smtk::resource::PersistentObjectSet result;
  return result;
}

void SubphraseGenerator::subphrasesForCreatedObjects(
    const smtk::resource::PersistentObjectArray& objects,
    const smtk::view::DescriptivePhrasePtr& localRoot,
    PhrasesByPath& resultingPhrases)
{
  // Add phrases underneath the fixed NodeGroupPhraseContent instances.
  // These nodes are under each resource, so we build a map of their locations first.
  struct Entry
  {
    SubphraseGenerator::Path path;
    std::shared_ptr<smtk::view::DescriptivePhrase> phrase;
  };
  std::map<smtk::resource::Resource*, std::map<std::string, Entry>> nodeTypes;

  // Scan for NodeGroupPhraseContent entries and add them to the map.
  int ii = -1;
  for (const auto& resourcePhrase : localRoot->subphrases())
  {
    ++ii;
    auto* resource = resourcePhrase->relatedResource().get();
    if (!resource) { continue; }
    std::map<std::string, Entry> blank;
    nodeTypes[resource] = blank;
    int jj = -1;
    for (const auto& subphrase : resourcePhrase->subphrases())
    {
      ++jj;
      if (auto* nodeGroup = dynamic_cast<sketch::NodeGroupPhraseContent*>(subphrase->content().get()))
      {
        nodeTypes[resource][nodeGroup->childType()] = Entry{{ii, jj}, subphrase};
      }
    }
  }

  // Now we can look up where to add each object.
  smtk::resource::PersistentObjectSet result;
  auto phrasesForObject(localRoot->phraseModel()->uuidPhraseMap()); // TODO: Fix SMTK to return const ref.
  std::vector<int> objectPath;
  for (const auto& object : objects)
  {
    auto node = std::dynamic_pointer_cast<sketch::Node>(object);
    if (!node)
    {
      continue;
    }
    auto* resource = node->parentResource();
    if (!resource)
    {
      continue;
    }
    auto resourceIt = nodeTypes.find(resource);
    if (resourceIt == nodeTypes.end())
    {
      continue;
    }
    std::string quotedTypeName = "'" + node->typeName() + "'";
    auto nodeTypeIt = resourceIt->second.find(quotedTypeName);
    if (nodeTypeIt == resourceIt->second.end())
    {
      continue;
    }

    // TODO: Compute the path of object underneath (1) the appropriate
    //       NodeGroupPhraseContent node using nodeTypeIt and (2) any
    //       other locations where the object must appear. For each
    //       path, create a descriptive phrase and append the pair to
    //       the resultingPhrases.
    std::cout << "TODO: Implement SubphraseGenerator::subphrasesForCreatedObjects().\n";
  }
}

SubphraseGenerator::PhrasePath SubphraseGenerator::indexOfObjectInParent(
  const smtk::resource::PersistentObjectPtr& obj,
  const smtk::view::DescriptivePhrasePtr& parent,
  const PhrasePath& parentPath)
{
  const auto& subphrases = parent->subphrases();
  // TODO: We could bisect here (since phrases are assumed ordered).
  //       That would be much faster than this O(N) algorithm.

  SubphraseGenerator::PhrasePath result = parentPath;
  std::string objName = obj->name();
  int ii = 0;
  for (const auto& phrase : subphrases)
  {
    if (smtk::common::StringUtil::mixedAlphanumericComparator(objName, phrase->title()))
    {
      result.push_back(ii);
      return result;
    }
    ++ii;
  }
  result.push_back(static_cast<int>(subphrases.size()));
  return result;
}

} // namespace sketch
