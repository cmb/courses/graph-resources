Step 4 Exercises
----------------

These exercises cover the use of descriptive phrases to present a
hierarchical view of your components.
As with step 3, the result of these exercises is a plugin
that can be loaded into modelbuilder.

0. **Unload the step-3 plugin**.
   To avoid conflicts, run modelbuilder and use the Tools→Manage Plugins… menu
   to remove the plugin.

1. **New phrase content**.
   We want to present a hierarchy that has a fixed set of phrases
   beneath our graph resource: cusps, groups, paths, and styles.
   Nodes then appear underneath each of these fixed phrases.
   Finally, nodes that are shapes may appear underneath groups
   (if they are members of the group) and styles (if they are marked
   with the style); and cusps will appear under each path node they bound.

   To have a set of fixed nodes, we'll add a NodeGroupPhraseContent class.
   Edit the ``sketch/NodeGroupPhraseContent.cxx`` file and finish the
   ``setup()`` and ``stringValue()`` methods.

2. **New subphrase generator**.
   In order for the new phrase to appear in a phrase model, we must have
   a subphrase generator that creates it.
   Edit the ``sketch/SubphraseGenerator.cxx`` file and finish the
   ``subphrases()``, ``hasChildren()``, ``parentObjects()``,
   ``subphrasesForCreatedObjects()`` methods as directed by the comments.

3. **Application-specific panel configuration**.
   Finally, we need to tell the resource panel to use our custom subphrase
   generator when the application starts.
   See ``sketch/ApplicationConfiguration.cxx`` for how this is done.
   Once you see this, build the plugin and use modelbuilder's plugin
   manager to load the step-4 plugin. Set the plugin to auto-load by
   default and then restart modelbuilder.
   (This causes the plugin to load earlier during the startup process and
   is useful for ensuring initialization happens without duplicated effort.)

Run modelbuilder and load the example SMTK file to see the plugin in action!
