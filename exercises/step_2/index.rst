Step 2 Exercises
----------------

0. We are beginning this problem set where step 1 left off;
   we have a set of node types and explicit arc types.
   Each of the node classes has been moved to its own header file.

   This time, we will add an implicit arc type and
   implement JSON serialization/deserialization.

   Run the ``StartingPoint2`` exercise by passing ``StartingPoint2`` to
   the executable created for all the exercises.
   Note the **TODO** printouts and locate them in the source
   files in this directory.

1. **Add an implicit arc**.
   The skeleton of an implicit arc type is in ``sketch/DefaultStyle.h`` and
   ``sketch/DefaultStyle.txx``. The header is included by ``sketch/Traits.h`` as are
   the node-type headers.

   The arc is named ``DefaultStyle`` and connects ``Shape`` nodes which have no
   incoming ``StylesToShapes`` arc to the resource's default style node (if
   a default exists).

   a. Modify the ``sketch/DefaultStyle.txx`` file to finish the implementation
      where the print statement indicates.
      To do this, you'll need to implement methods on the resource
      class to set and get the default style node. ``Exercise2_1.cxx``
      calls these methods for you.
   b. Modify ``Exercise2_1.cxx`` so that some of the Path nodes
      are not assigned a style. How does this affect the arcs dumped to
      std::cout when you run the program?

2. **Add JSON serialization/deserialization**.
   Modify the source files in this directory,
   replacing the printouts above with implementations for creating JSON
   objects from nodes and vice versa.

   The ``sketch/json/jsonResource.{h,cxx}`` files contain a skeleton for
   serialization and deserialization, but need changes as does ``sketch/Node.h``.

   Are implicit arcs serialized? explicit arcs?
   How would you change which arcs are serialized?
   What do you need to add to indicate nodes should be serialized?
