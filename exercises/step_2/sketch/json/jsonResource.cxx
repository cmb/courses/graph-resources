// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "sketch/json/jsonResource.h"

#include "sketch/Node.h"
#include "sketch/DefaultStyle.txx"

#include "smtk/common/json/jsonTypeMap.h"
#include "smtk/resource/json/jsonResource.h"
#include "smtk/resource/json/Helper.h"
#include "smtk/graph/json/jsonResource.h"
#include "smtk/string/Manager.h"
#include "smtk/string/json/jsonManager.h"

using ArcMap = smtk::graph::ArcMap;

namespace sketch
{

// This method is used to serialize Nodes and all subclasses
// that do not have their own to_json implementation.
//
// You may overload this function and the exact node type
// is used at runtime to call the correct overload.
//
// Your overloaded versions for subclasses may call this
// method and then perform additional work.
void to_json(nlohmann::json& jj, const sketch::Node* node)
{
  (void)jj;
  (void)node;
  std::cout << "** TODO ** Implement a serializer for Nodes.\n";
  // Assign a value to jj holding data relevant to each node.
}

// This template is used to deserialize nodes of all types.
// If you need to perform processing specific to one type,
// specialize this templated method.
template<typename NodeType>
void from_json(const json& jj, std::shared_ptr<NodeType>& node)
{
  auto helper = smtk::resource::json::Helper::instance();
  auto resource = std::dynamic_pointer_cast<smtk::graph::ResourceBase>(helper.resource());
  if (resource)
  {
    std::cout << "** TODO ** Implement a deserializer for Node types.\n";
  }
}

void to_json(nlohmann::json& jj, const sketch::Resource::Ptr& resource)
{
  std::cout << "** TODO ** Implement a serializer for sketch::Resource.\n";
  // First, call the "parent" class' to_json() implementation
  // to serialize member data inherited from our parent class.
  // Then populate jj with anything particular to our resource.
  // For this example, we need to save the default style node (if any).
}

void from_json(const nlohmann::json& jj, sketch::Resource::Ptr& resource)
{
  // Since this method may be invoked by other functions that have
  // already created an output resource, we use SMTK's smtk::resource::json::Helper
  // to fetch the output resource if it exists. If not, tell the helper
  // about the resource we create so other methods involved in deserialization
  // can use it (specifically, the node deserializer).
  if (!resource)
  {
    resource = std::dynamic_pointer_cast<sketch::Resource>(
      smtk::resource::json::Helper::instance().resource());
    if (!resource)
    {
      resource = sketch::Resource::create();
      smtk::resource::json::Helper::pushInstance(resource);
    }
  }

  std::cout << "** TODO ** Implement a deserializer for sketch::Resource.\n";
  // Now, call the parent class's from_json method.

  // Finally, serialize any members specific to this resource.
}

} // namespace sketch
