// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Shape_h
#define sketch_step2_Shape_h

#include "sketch/Node.h"

namespace sketch
{

class SKETCHSTEP2_EXPORT Shape : public Node
{
public:
  smtkTypeMacro(sketch::Shape);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Shape(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

} // namespace sketch

#endif // sketch_step2_Shape_h
