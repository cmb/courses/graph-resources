// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_DefaultStyle_txx
#define sketch_step2_DefaultStyle_txx

#include "sketch/Resource.h"

namespace sketch
{

template<typename Functor>
smtk::common::Visited DefaultStyle::outVisitor(const Shape* shape, Functor ff) const
{
  (void) ff;
  if (!shape)
  {
    return smtk::common::Visited::Empty;
  }

  // TODO: Implement a method to invoke \a ff on a Style node
  //       The style should be obtained from shape->parentResource()
  //       (i.e., it is the default style for that resource).
  //       If the resource has no default style, then simply
  //       return smtk::common::Visited::Empty.
  std::cout << "    ** TODO ** Implement a method to invoke ff on a Style node\n";
  return smtk::common::Visited::Empty;
}

} // namespace sketch

#endif // sketch_step2_DefaultStyle_txx
