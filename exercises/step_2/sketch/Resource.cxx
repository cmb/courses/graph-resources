// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "sketch/Resource.h"

namespace sketch
{

Resource::Resource(const smtk::common::UUID& uid, smtk::resource::Manager::Ptr manager)
  : Superclass(uid, manager)
{
  this->initialize();
}

Resource::Resource(smtk::resource::Manager::Ptr manager)
  : Superclass(manager)
{
  this->initialize();
}

std::function<bool(const smtk::resource::Component&)> Resource::queryOperation(
  const std::string& query) const
{
  // TODO: We could add grammar here instead of passing it to our parent.
  return this->Superclass::queryOperation(query);
}

void Resource::setDefaultStyle(const std::shared_ptr<Style>& style)
{
  // TODO: Implement me
  std::cout << "** TODO ** Set default rendering style for resource\n";
}

const Style* Resource::defaultStyle() const
{
  std::cout << "** TODO ** Get default rendering style of resource\n";
  return nullptr; // TODO: Replace this with an implementation.
}

void Resource::initialize()
{
  // TODO: Register any queries here.
}

} // namespace sketch
