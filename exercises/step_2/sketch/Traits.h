// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Traits_h
#define sketch_step2_Traits_h

#include "sketch/Exports.h"

#include "smtk/graph/Component.h"

#include <type_traits> // for std::true_type
#include <tuple>
#include <vector>

namespace sketch
{

// Forward-declare node types

class Cusp;
class Group;
class Path;
class Shape;
class Style;

// Declare arc traits

/// Paths can be grouped together.
struct SKETCHSTEP2_EXPORT GroupsToPaths
{
  using FromType = Group;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 group.
};

/// Paths can be smooth, closed curves (no cusps) or have cusps (at ends or along interior).
struct SKETCHSTEP2_EXPORT PathsToCusps
{
  using FromType = Path;
  using ToType = Cusp;
  using Directed = std::true_type;
};

/// Paths can have a style.
struct SKETCHSTEP2_EXPORT StylesToShapes
{
  using FromType = Style;
  using ToType = Shape;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 style.
};

struct DefaultStyle;

/// Node and arc types for hand drawings.
struct SKETCHSTEP2_EXPORT Traits
{
  // Note we do not need to list Node and Shape types in NodeTypes; they are not allowed
  // to exist in the graph... only their subclasses are.
  using NodeTypes = std::tuple<Cusp, Group, Path, Style>;
  using ArcTypes = std::tuple<GroupsToPaths, PathsToCusps, StylesToShapes, DefaultStyle>;
};

} // namespace sketch

// Practically anything that uses the traits types needs access to
// the declarations of the node and arc types; include node types here.
#include "sketch/Cusp.h"
#include "sketch/Group.h"
#include "sketch/Node.h"
#include "sketch/Path.h"
#include "sketch/Shape.h"
#include "sketch/Style.h"

// Our implicit arc's implementation is not just a few type aliases,
// so we split it into its own file. Furthermore, its methods require
// access to the Resource type (which is templated on the Traits class
// in this header), so the methods themselves are kept in a separate txx file.
#include "sketch/DefaultStyle.h"

#endif // sketch_step2_Traits_h
