// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Style_h
#define sketch_step2_Style_h

#include "sketch/Node.h"

namespace sketch
{

/// A node representing the drawing style for a path (e.g. color, thickness, ...)
class SKETCHSTEP2_EXPORT Style : public Node
{
public:
  smtkTypeMacro(sketch::Style);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Style(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  void setLineColor(const std::array<double, 4>& color)
  {
    m_lineColor = color;
  }

  void setLineThickness(double thickness)
  {
    this->properties().get<double>()["line_thickness"] = thickness;
  }

protected:
  std::array<double, 4> m_lineColor{0.9, 0.9, 0.9, 1.0};
};

} // namespace sketch

#endif // sketch_step2_Style_h
