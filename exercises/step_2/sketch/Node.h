// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Node_h
#define sketch_step2_Node_h

#include "sketch/Exports.h"
#include "smtk/graph/Component.h"

namespace sketch
{

class SKETCHSTEP2_EXPORT Node : public smtk::graph::Component
{
public:
  smtkTypeMacro(sketch::Node);
  smtkSuperclassMacro(smtk::graph::Component);
  /// TODO: Mark this node (and by default, its subclasses) for JSON serialization:

  template<typename... Args>
  Node(Args&&... args)
    : smtk::graph::Component(std::forward<Args>(args)...)
  {
  }

  std::string name() const override
  {
    if (this->properties().contains<std::string>("name"))
    {
      return this->properties().at<std::string>("name");
    }
    return this->Superclass::name();
  }
  void setName(const std::string& nodeName)
  {
    this->properties().get<std::string>()["name"] = nodeName;
  }
};


} // namespace sketch

#endif // sketch_step2_Node_h
