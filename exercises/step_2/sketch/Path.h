// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Path_h
#define sketch_step2_Path_h

#include "sketch/Shape.h"

namespace sketch
{

/// A node representing a single stroke of a pen.
class SKETCHSTEP2_EXPORT Path : public Shape
{
public:
  smtkTypeMacro(sketch::Path);
  smtkSuperclassMacro(sketch::Shape);

  template<typename... Args>
  Path(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

} // namespace sketch

#endif // sketch_step2_Path_h
