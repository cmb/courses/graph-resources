---------
Exercises
---------

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   step_1/index.rst
   step_2/index.rst
   step_3/index.rst
