=======================
Graph Resource Tutorial
=======================

Welcome to the graph resource tutorial!
This covers how to build a graph resource from the ground up.

Each ``exercises/step_N`` directory has code as a starting point
with some pieces left for you to fill in.
Each ``solutions/step_N`` directory has one possible solution to
all the problems inside the corresponding exercise.
Don't peek unless you are really stumped!

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   exercises/index.rst
   solutions/index.rst

.. role:: cxx(code)
   :language: c++
