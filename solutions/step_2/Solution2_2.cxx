#include "sketch/Resource.h"
#include "sketch/DefaultStyle.txx"

#include "sketch/json/jsonResource.h"

#include <memory>

int Solution2_2(int, char**)
{
  using GroupsToPaths = sketch::GroupsToPaths;
  using PathsToCusps = sketch::PathsToCusps;
  using StylesToShapes = sketch::StylesToShapes;

  auto resource = sketch::Resource::create();
  resource->setName("example");

  // Create some nodes.
  auto path1 = resource->create<sketch::Path>();
  auto path2 = resource->create<sketch::Path>();
  auto group = resource->create<sketch::Group>();
  auto plain = resource->create<sketch::Style>();
  auto fancy = resource->create<sketch::Style>();
  path1->setName("path 1");
  path2->setName("path 2");
  group->setName("group");
  plain->setName("plain style");
  fancy->setLineColor({0.6, 0.2, 0.2});
  fancy->setLineThickness(2.0);
  fancy->setName("fancy style");
  resource->setDefaultStyle(plain);

  // Add some cusp nodes
  auto cusp1 = resource->create<sketch::Cusp>();
  auto cusp2 = resource->create<sketch::Cusp>();
  cusp1->setName("endpoint 1");
  cusp2->setName("endpoint 2");
  path1->outgoing<PathsToCusps>().connect(cusp1);
  path1->outgoing<PathsToCusps>().connect(cusp2);

  // Set point coordinates on the cusps:
  cusp1->setCoordinates({1.0, 0.0});
  cusp2->setCoordinates({2.0, 1.0});

  // Connect the nodes into a graph.
  group->outgoing<GroupsToPaths>().connect(path1);
  group->outgoing<GroupsToPaths>().connect(path2);
  plain->outgoing<StylesToShapes>().connect(path1);
  fancy->outgoing<StylesToShapes>().connect(path2);

  // Instead of calling dump() on the resulting graph,
  // we'll convert the resource to JSON and dump a
  // nicely-indented JSON string:
  nlohmann::json jsonResource = resource;
  std::cout << jsonResource.dump(2) << "\n";

  // Now, let's finish the round-trip by converting
  // the json back into a sketch::Resource.
  sketch::Resource::Ptr resource2 = jsonResource;
  if (!resource2)
  {
    std::cerr << "Could not convert JSON into a resource!\n";
    return 1;
  }
  std::cout << "***\n*** Dumping the round-tripped resouce\n***\n";
  resource2->dump("", "text/vnd.graphviz");

  return 0;
}
