Step 2 Solutions
----------------

0. There are 2 printouts:
   a. The first is `Set default rendering style for resource` in Resource.cxx.
   b. The second is `Implement a method to invoke ff on a Style node` in Traits.h

1. Note that our solution holds a weak pointer to the default style.
   This way, if the style node that is default is removed from the resource,
   our default style will automatically be reset. It is very important to
   pay attention to ownership semantics in SMTK; holding shared pointers when
   they are not absolutely required can cause counter-intuitive behavior and
   be hard to debug.

   The solution's implementation of the ``outVisitor()`` method is a common
   pattern in SMTK for dealing with visitors that may or may not wish to
   terminate early. Visitor functors that do not wish to exit iteration
   need not return a value. Functors that do wish to exit early should
   return an :smtk:`smtk::common::Visit` enumerant.
   The :smtk:`smtk::common::VisitorFunctor` template is an adaptor that can
   accept either type of functor.

   .. figure:: exercise1.png

      This is what the result should look like.

   At first, ``Exercise2_1`` should have 2 ``DefaultStyle`` arcs listed
   (since the 2 Cusp nodes have no style assigned).
   Once you comment out more of the style assignments, you should see
   those shapes listed with a default style. If you comment out the
   ``resource->setDefaultStyle()`` call in the exercise, the implicit
   arcs will no longer appear.

2. TODO: FIXME: URHERE.
