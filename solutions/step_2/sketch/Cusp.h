// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Cusp_h
#define sketch_step2_Cusp_h

#include "sketch/Shape.h"

namespace sketch
{

/// A node representing a C-0 point on a path (i.e., a corner or endpoint).
class SKETCHSTEP2_SOLVED_EXPORT Cusp : public Shape
{
public:
  smtkTypeMacro(sketch::Cusp);
  smtkSuperclassMacro(sketch::Shape);

  template<typename... Args>
  Cusp(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
  /// Set coordinates on a cusp node using SMTK's property system.
  void setCoordinates(const std::vector<double>& point)
  {
    auto& coords = this->properties().get<std::vector<double>>()["coordinates"];
    coords = point;
  }

  /// Fetch coordinates on cusp node using SMTK's property system.
  const std::vector<double>& coordinates() const
  {
    if (this->properties().contains<std::vector<double>>("coordinates"))
    {
      return this->properties().at<std::vector<double>>("coordinates");
    }
    // If the property is not present (because no coordinates have been set),
    // return a blank vector rather than crash trying to dereference a
    // non-existent entry.
    static std::vector<double> dummy;
    return dummy;
  }
};

} // namespace sketch

#endif // sketch_step2_Cusp_h
