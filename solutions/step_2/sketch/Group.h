// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_Group_h
#define sketch_step2_Group_h

#include "sketch/Node.h"

namespace sketch
{

/// A node representing a collection of paths.
class SKETCHSTEP2_SOLVED_EXPORT Group : public Node
{
public:
  smtkTypeMacro(sketch::Group);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Group(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

} // namespace sketch

#endif // sketch_step2_Group_h
