// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step2_DefaultStyle_txx
#define sketch_step2_DefaultStyle_txx

#include "Resource.h"

namespace sketch
{

template<typename Functor>
smtk::common::Visited DefaultStyle::outVisitor(const Shape* shape, Functor ff) const
{
  auto result = smtk::common::Visited::Empty;
  if (!shape || !shape->incoming<StylesToShapes>().empty())
  {
    return result;
  }
  smtk::common::VisitorFunctor<Functor> visitor(ff);
  if (const auto* defaultStyle =
    dynamic_cast<sketch::Resource*>(shape->parentResource())->defaultStyle())
  {
    result =
      (visitor(defaultStyle) == smtk::common::Visit::Halt) ?
      smtk::common::Visited::Some : smtk::common::Visited::All;
  }
  return result;
}

} // namespace sketch

#endif // sketch_step2_DefaultStyle_txx
