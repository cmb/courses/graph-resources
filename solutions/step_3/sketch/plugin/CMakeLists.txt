smtk_add_plugin(smtkSketchStep3SolvedPlugin
  REGISTRARS
    sketch::Registrar
  MANAGERS
    smtk::operation::Manager
    smtk::resource::Manager
    smtk::geometry::Manager
    # smtk::view::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION 1.0
)

target_link_libraries(smtkSketchStep3SolvedPlugin
  PRIVATE
    sketchStep3
    # vtkOpencascadeGeometryExt
    vtkSMTKGeometryExt
    smtkCore
)
