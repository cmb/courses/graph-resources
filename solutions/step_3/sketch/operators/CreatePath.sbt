<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Definitions>

    <!-- Parameters -->
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="create path" Label="new path" BaseType="operation">

      <AssociationsDef LockType="Write" OnlyResources="true" NumberOfRequiredValues="1">
        <BriefDescription>The resource to add the shape to.</BriefDescription>
        <Accepts><Resource Name="sketch::Resource"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Group Name="curve" NumberOfRequiredGroups="1">

          <ItemDefinitions>
            <Double Name="points" NumberOfRequiredValues="9" Extensible="true"/>
            <Void Name="closed" IsEnabledByDefault="false"/>
          </ItemDefinitions>

        </Group>

      </ItemDefinitions>

    </AttDef>

    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(create path)" BaseType="result">
      <ItemDefinitions>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
  <Views>
    <View Type="Operation" Title="create path"
      FilterByAdvanceLevel="false" UseSelectionManager="true">
      <InstancedAttributes>
        <Att Type="create path" Name="create path">
          <ItemViews>
            <View Item="curve" Type="Spline" Points="points" Closed="closed" ShowControls="true"/>
          </ItemViews>
        </Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
