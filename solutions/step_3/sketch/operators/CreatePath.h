//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_CreatePath_h
#define sketch_CreatePath_h

#include "sketch/Resource.h"

#include "smtk/operation/XMLOperation.h"

namespace sketch
{

/**\brief Create a path given a set of points.
  */
class SKETCHSTEP3_SOLVED_EXPORT CreatePath : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(sketch::CreatePath);
  smtkCreateMacro(CreatePath);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
};

} // namespace sketch

#endif // sketch_CreatePath_h
