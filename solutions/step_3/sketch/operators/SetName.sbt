<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the sketch resource's "create" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="set name" Label="set name" BaseType="operation">
      <AssociationsDef LockType="Write">
        <Accepts>
          <!-- Accept sketch resources -->
          <Resource Name="sketch::Resource"/>
          <!-- Accept any sketch component -->
          <Resource Name="sketch::Resource" Filter="*"/>
        </Accepts>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="name" Label="name">
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(set name)" BaseType="result">
      <ItemDefinitions>

        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="sketch::Resource"/>
          </Accepts>
        </Resource>

      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
