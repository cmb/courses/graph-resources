//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_Read_h
#define sketch_Read_h

#include "sketch/Resource.h"

#include "smtk/operation/XMLOperation.h"

namespace sketch
{

/**\brief Read a sketch resource.
  */
class SKETCHSTEP3_SOLVED_EXPORT Read : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(sketch::Read);
  smtkCreateMacro(Read);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

  bool ableToOperate() override;

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;
};

SKETCHSTEP3_SOLVED_EXPORT smtk::resource::ResourcePtr read(
  const std::string& filename,
  const std::shared_ptr<smtk::common::Managers>& managers = nullptr);

} // namespace sketch

#endif // sketch_Read_h
