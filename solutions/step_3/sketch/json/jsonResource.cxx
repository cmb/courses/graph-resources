// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "sketch/json/jsonResource.h"

#include "sketch/Node.h"
#include "sketch/DefaultStyle.txx"

#include "smtk/common/json/jsonTypeMap.h"
#include "smtk/resource/json/jsonResource.h"
#include "smtk/resource/json/Helper.h"
#include "smtk/graph/json/jsonResource.h"
#include "smtk/string/Manager.h"
#include "smtk/string/json/jsonManager.h"

using ArcMap = smtk::graph::ArcMap;

namespace sketch
{

// This method is used to serialize Nodes and all subclasses
// that do not have their own to_json implementation.
//
// You may overload this function and the exact node type
// is used at runtime to call the correct overload.
//
// Your overloaded versions for subclasses may call this
// method and then perform additional work.
void to_json(nlohmann::json& jj, const sketch::Node* node)
{
  if (!node)
  {
    throw std::invalid_argument("Null node.");
  }
  // The only thing we need to create a node is its UUID:
  jj["id"] = node->id();
}

void to_json(nlohmann::json& jj, const sketch::Style* style)
{
  to_json(jj, static_cast<const sketch::Node*>(style));
  jj["line_color"] = style->lineColor();
}

void to_json(nlohmann::json& jj, const sketch::Path* path)
{
  // to_json(jj, static_cast<const sketch::Node*>(path));
  jj["id"] = path->id();
  json::array_t geom;
  auto points = path->points();
  if (!points.empty())
  {
    jj["geometry"] = points;
    jj["closed"] = path->closed();
  }
}

// This template is used to deserialize nodes of all types.
// If you need to perform processing specific to one type,
// specialize this templated method.
template<typename NodeType>
void from_json(const json& jj, std::shared_ptr<NodeType>& node)
{
  auto helper = smtk::resource::json::Helper::instance();
  auto resource = std::dynamic_pointer_cast<smtk::graph::ResourceBase>(helper.resource());
  if (resource)
  {
    // Construct a node of the proper type with its resource and UUID set.
    // Note that you must provide a constructor that passes these arguments
    // to the base graph-resource component class or you will have build errors.
    node = std::make_shared<NodeType>(resource, jj.at("id").get<smtk::common::UUID>());
    // Adding the node can fail if the node's type is disallowed by the resource.
    if (!resource->addNode(node))
    {
      smtkErrorMacro(smtk::io::Logger::instance(), "Could not add node.");
    }
  }
}

template<>
void from_json<sketch::Style>(const json& jj, std::shared_ptr<Style>& style)
{
  auto helper = smtk::resource::json::Helper::instance();
  auto resource = std::dynamic_pointer_cast<smtk::graph::ResourceBase>(helper.resource());
  if (resource)
  {
    // Construct a node of the proper type with its resource and UUID set.
    // Note that you must provide a constructor that passes these arguments
    // to the base graph-resource component class or you will have build errors.
    style = std::make_shared<sketch::Style>(resource, jj.at("id").get<smtk::common::UUID>());
    auto pit = jj.find("line_color");
    if (pit != jj.end())
    {
      auto color = pit->get<std::array<double, 4>>();
      style->setLineColor(color);
    }
    // Adding the style can fail if the style's type is disallowed by the resource.
    if (!resource->addNode(style))
    {
      smtkErrorMacro(smtk::io::Logger::instance(), "Could not add style.");
    }
  }
}

template<>
void from_json<sketch::Path>(const json& jj, std::shared_ptr<Path>& path)
{
  auto helper = smtk::resource::json::Helper::instance();
  auto resource = std::dynamic_pointer_cast<smtk::graph::ResourceBase>(helper.resource());
  if (resource)
  {
    // Construct a node of the proper type with its resource and UUID set.
    // Note that you must provide a constructor that passes these arguments
    // to the base graph-resource component class or you will have build errors.
    path = std::make_shared<sketch::Path>(resource, jj.at("id").get<smtk::common::UUID>());
    auto pit = jj.find("geometry");
    if (pit != jj.end())
    {
      auto pts = pit->get<std::vector<double>>();
      path->setPoints(pts, jj.at("closed").get<bool>());
    }
    // Adding the path can fail if the path's type is disallowed by the resource.
    if (!resource->addNode(path))
    {
      smtkErrorMacro(smtk::io::Logger::instance(), "Could not add path.");
    }
  }
}

void to_json(nlohmann::json& jj, const sketch::Resource::Ptr& resource)
{
  // Add version number, arcs, nodes, properties, and other information
  // inherited from our parent graph-resource.
  smtk::graph::to_json(jj, std::static_pointer_cast<sketch::SketchResource>(resource));

  // Populate jj with anything particular to our resource.
  // For this example, we need to save the default style node (if any).
  const auto* defStyle = resource->defaultStyle();
  if (defStyle)
  {
    jj["default_style"] = defStyle->id();
  }
}

void from_json(const nlohmann::json& jj, sketch::Resource::Ptr& resource)
{
  if (!resource)
  {
    resource = std::dynamic_pointer_cast<sketch::Resource>(
      smtk::resource::json::Helper::instance().resource());
    if (!resource)
    {
      resource = sketch::Resource::create();
      smtk::resource::json::Helper::pushInstance(resource);
    }
  }

  // Deserialize arcs and nodes using smtk::graph::from_json()
  auto tmp = std::static_pointer_cast<sketch::SketchResource>(resource);
  smtk::graph::from_json(jj, tmp);

  // If the JSON provides a default style, set it:
  auto it = jj.find("default_style");
  if (it != jj.end())
  {
    auto defStyle = std::dynamic_pointer_cast<sketch::Style>(
      resource->find(it->get<smtk::common::UUID>()));
    resource->setDefaultStyle(defStyle);
  }
}

} // namespace sketch
