// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step3_solved_queries_SelectionFootprint_h
#define sketch_step3_solved_queries_SelectionFootprint_h

#include "sketch/Exports.h"

#include "smtk/geometry/queries/SelectionFootprint.h"

namespace sketch
{
namespace queries
{

class SKETCHSTEP3_SOLVED_EXPORT SelectionFootprint
  : public smtk::resource::query::DerivedFrom<
    SelectionFootprint, smtk::geometry::SelectionFootprint>
{
public:
  bool operator()(
    smtk::resource::PersistentObject& selectedObject,
    std::unordered_set<smtk::resource::PersistentObject*>& footprint,
    const smtk::geometry::Backend& backend) const override;
};

} // namespace queries
} // namespace sketch

#endif // sketch_step3_solved_queries_SelectionFootprint_h
