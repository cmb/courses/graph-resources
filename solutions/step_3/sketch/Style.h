// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step3_solved_Style_h
#define sketch_step3_solved_Style_h

#include "Node.h"

namespace sketch
{

/// A node representing the drawing style for a path (e.g. color, thickness, ...)
class SKETCHSTEP3_SOLVED_EXPORT Style : public Node
{
public:
  smtkTypeMacro(sketch::Style);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Style(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  /// Set/get a color to use when rendering lines.
  ///
  /// Note that this is stored as a member variable - not a property - while
  /// line thickness is stored as a property. This was done for illustrative
  /// purposes.
  void setLineColor(const std::array<double, 4>& color)
  {
    m_lineColor = color;
  }
  const std::array<double, 4>& lineColor() const { return m_lineColor; }

  /// Set/get a thickness to use for lines when rendering.
  void setLineThickness(double thickness)
  {
    this->properties().get<double>()["line_thickness"] = thickness;
  }
  double lineThickness() const
  {
    if (this->properties().contains<double>("line_thickness"))
    {
      return this->properties().at<double>("line_thickness");
    }
    return -1.0;
  }

protected:
  std::array<double, 4> m_lineColor{0.9, 0.9, 0.9, 1.0};
};

} // namespace sketch

#endif // sketch_step3_solved_Style_h
