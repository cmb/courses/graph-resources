#include "sketch/Registrar.h"
#include "sketch/Resource.h"
#include "sketch/DefaultStyle.txx"

#include "sketch/json/jsonResource.h"

#include "smtk/resource/Manager.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/groups/NamingGroup.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/plugin/Registry.h"

#include <memory>

int Solution3_1(int, char**)
{
  using GroupsToPaths = sketch::GroupsToPaths;
  using PathsToCusps = sketch::PathsToCusps;
  using StylesToShapes = sketch::StylesToShapes;

  auto resourceManager = smtk::resource::Manager::create();
  auto operationManager = smtk::operation::Manager::create();
  // Register the sketch resource type and operations with the managers above:
  smtk::plugin::Registry<
    sketch::Registrar, smtk::resource::Manager, smtk::operation::Manager
  > registry(resourceManager, operationManager);

  // Register the resource manager to the operation manager (so resources
  // referenced in operation results will be automatically registered to
  // the resource manager).
  operationManager->registerResourceManager(resourceManager);

  // Ask the resource manager to create our resource instead of creating it ourselves.
  // This will call the create() free function, which in turn runs a Create() operation.
  auto resource = resourceManager->create<sketch::Resource>();
  if (!resource)
  {
    std::cout << "Stopping until resource creation works.\n";
    return 0;
  }
  resource->setName("example");

  // Create some nodes.
  auto path1 = resource->create<sketch::Path>();
  auto path2 = resource->create<sketch::Path>();
  auto group = resource->create<sketch::Group>();
  auto plain = resource->create<sketch::Style>();
  auto fancy = resource->create<sketch::Style>();
  path1->setName("path 1");
  path2->setName("path 2");
  group->setName("group");
  plain->setName("plain style");
  plain->setLineColor({0.5, 0.7, 0.7, 1.0});
  fancy->setLineColor({0.7, 0.5, 0.5, 1.0});
  fancy->setLineThickness(2.0);
  fancy->setName("fancy style");
  resource->setDefaultStyle(plain);

  // Add some cusp nodes
  auto cusp1 = resource->create<sketch::Cusp>();
  auto cusp2 = resource->create<sketch::Cusp>();
  cusp1->setName("endpoint 1");
  cusp2->setName("endpoint 2");
  path1->outgoing<PathsToCusps>().connect(cusp1);
  path1->outgoing<PathsToCusps>().connect(cusp2);

  // Set point coordinates on the cusps:
  cusp1->setCoordinates({1.0, 0.0, 0.0});
  cusp2->setCoordinates({2.0, 1.0, 0.0});
  path1->setPoints({1.0, 0.0, 0.0, 2.0, 1.0, 0.0}, /* closed */false);
  path2->setPoints({0., 0., 0.,   0.5, 0., 0.,   0., 0.5, 0.}, /* closed */true);

  // Connect the nodes into a graph.
  group->outgoing<GroupsToPaths>().connect(path1);
  group->outgoing<GroupsToPaths>().connect(path2);
  plain->outgoing<StylesToShapes>().connect(path1);
  fancy->outgoing<StylesToShapes>().connect(path2);

  // Instead of directly converting to JSON, we'll
  // have the resource manager save the resource
  // (which writes JSON to the resource->location()).
  resourceManager->write(resource, "example.smtk", nullptr);

  // Now, let's finish the round-trip by dropping
  // the resource from the resource manager, resetting
  // our local shared pointer to it (which should evict
  // the resource from memory) and the load it back in.
  resourceManager->remove(resource);
  std::cout << "resource use_count just before reset: " << resource.use_count() << "\n";
  resource = nullptr;

  // Load the resource back from disk:
  resource = std::dynamic_pointer_cast<sketch::Resource>(
    resourceManager->read("sketch::Resource", "example.smtk", nullptr));
  if (!resource)
  {
    std::cerr << "Could not re-load resource!\n";
    return 1;
  }
  std::cout << "***\n*** Dumping the round-tripped resouce\n***\n";
  resource->dump("", "text/vnd.graphviz");

  return 0;
}
