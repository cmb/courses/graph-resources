#include "sketch/queries/SelectionFootprint.h"

#include "sketch/Traits.h"

namespace sketch
{
namespace queries
{

bool SelectionFootprint::operator()(
  smtk::resource::PersistentObject& selectedObject,
  std::unordered_set<smtk::resource::PersistentObject*>& footprint,
  const smtk::geometry::Backend& backend) const
{
  bool haveFootprint = false;
  if (auto* path = dynamic_cast<Path*>(&selectedObject))
  {
    haveFootprint = true;
    footprint.insert(path);
  }
  else if (auto* cusp = dynamic_cast<Cusp*>(&selectedObject))
  {
    haveFootprint = true;
    footprint.insert(cusp);
  }
  else if (auto* group = dynamic_cast<Group*>(&selectedObject))
  {
    haveFootprint = true;
    group->outgoing<GroupsToPaths>().visit(
      [&footprint](const Path* path)
      { footprint.insert(const_cast<Path*>(path)); }
    );
  }
  else if (auto* style = dynamic_cast<Style*>(&selectedObject))
  {
    haveFootprint = true;
    style->outgoing<StylesToShapes>().visit(
      [&footprint](const Shape* shape)
      { footprint.insert(const_cast<Shape*>(shape)); }
    );
  }
  else
  {
    haveFootprint = this->addAllComponentsIfResource(selectedObject, footprint, backend);
  }
  return haveFootprint;
}

} // namespace queries
} // namespace sketch
