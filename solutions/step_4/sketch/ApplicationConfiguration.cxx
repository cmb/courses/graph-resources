//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "sketch/ApplicationConfiguration.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKOperationToolboxPanel.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResourceBrowser.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResourcePanel.h"
#include "smtk/view/Configuration.h"
#include "smtk/view/Information.h"
#include "smtk/view/json/jsonView.h"

ApplicationConfiguration::ApplicationConfiguration(QObject* parent)
  : QObject(parent)
{
}

smtk::view::Information ApplicationConfiguration::panelConfiguration(const QWidget* panel)
{
  smtk::view::Information result;

  if (const auto* resources = dynamic_cast<const pqSMTKResourcePanel*>(panel))
  {
    nlohmann::json j = nlohmann::json::parse(pqSMTKResourceBrowser::getJSONConfiguration());
    // Overwrite the default subphrase generator with our new one:
    // NB: This is fragile; changes to pqSMTKResourceBrowser::getJSONConfiguration will break it:
    j[0]["Component"]["Children"][0]["Children"][0]["Attributes"]["Type"] =
      "sketch::SubphraseGenerator";
    smtk::view::ConfigurationPtr config = j[0];
    result.insert_or_assign(config);
  }
  else if (const auto* toolbox = dynamic_cast<const pqSMTKOperationToolboxPanel*>(panel))
  {
    nlohmann::json jsonConfig = { { "Name", "Operations" },
      { "Type", "qtOperationPalette" },
      { "Component",
        { { "Name", "Details" },
          { "Attributes",
            { { "SearchBar", true }, { "Title", "Tools" }, { "SubsetSort", "Precedence" } } },
          { "Children",
            { { { "Name", "Model" }, { "Attributes", { { "Autorun", "true" } } } } } } } } };
    std::shared_ptr<smtk::view::Configuration> viewConfig = jsonConfig;
    result.insert_or_assign(viewConfig);

    // auto operations = std::make_shared<smtk::session::aeva::OperationDecorator>();
    // Ensure we insert the decorator as a base-type shared-pointer
    // so it can be retrieved properly:
    // result.insert_or_assign(std::dynamic_pointer_cast<smtk::view::OperationDecorator>(operations));
  }
  else
  {
    smtkWarningMacro(smtk::io::Logger::instance(),
      "Ignoring panel named \"" << (panel ? panel->objectName().toStdString() : "null") << "\"\n");
  }
  return result;
}
