// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step4_Path_h
#define sketch_step4_Path_h

#include "Shape.h"

#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

namespace sketch
{

/// A node representing a single stroke of a pen.
class SKETCHSTEP4_SOLVED_EXPORT Path : public Shape
{
public:
  smtkTypeMacro(sketch::Path);
  smtkSuperclassMacro(sketch::Shape);

  template<typename... Args>
  Path(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
    , m_geometry(vtkSmartPointer<vtkPolyData>::New())
  {
  }

  /// Provide access to renderable geometry.
  ///
  /// The VTK data is the only place where the sequence of points
  /// that approximates the path is held.
  ///
  /// The vtkPolyData will be populated with vtkPoints and a single
  /// cell (a VTK_POLYLINE) that references the points in order.
  ///
  /// When you call methods that modify the returned vtkPolyData, you
  /// must call smtk::operation::MarkGeometry().markModified() to
  /// inform SMTK to update what is rendered.
  vtkSmartPointer<vtkPolyData> geometry() const { return m_geometry; }

  /// A convenience method for setting the path's points.
  ///
  /// This will reset m_geometry to the list of points provided.
  /// You must call smtk::operation::MarkGeometry().markModified()
  /// after this function.
  /// (It is not called inside setPoints because a shared pointer
  /// is currently required and shared pointers to an object may
  /// not be used within that object's constructor.)
  void setPoints(const std::vector<double>& coordinates, bool closed);

  /// A convenience method to fetch the path's points from its internal storage.
  std::vector<double> points() const;

  /// A convenience method that returns true if the path is periodic.
  bool closed() const;

protected:
  vtkSmartPointer<vtkPolyData> m_geometry;
};

} // namespace sketch

#endif // sketch_step4_Path_h
