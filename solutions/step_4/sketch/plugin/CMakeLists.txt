set(interfaces
  ApplicationConfiguration
)

smtk_add_plugin(smtkSketchStep4SolvedPlugin
  REGISTRARS
    sketch::Registrar
  MANAGERS
    smtk::operation::Manager
    smtk::resource::Manager
    smtk::geometry::Manager
    smtk::view::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION 1.0
    UI_INTERFACES ${interfaces}
)

target_link_libraries(smtkSketchStep4SolvedPlugin
  PRIVATE
    sketchStep4_solved
    # vtkOpencascadeGeometryExt
    vtkSMTKGeometryExt
    smtkCore
)
# This is needed in order for the plugin to
# find sketch/ApplicationConfiguration.h in
# order to initialize it as a ParaView interface:
target_include_directories(smtkSketchStep4SolvedPlugin
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/..
)
