//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_Geometry_h
#define sketch_Geometry_h

#include "sketch/Exports.h"

#include "smtk/extension/vtk/geometry/Geometry.h"
#include "smtk/geometry/Cache.h"
#include "smtk/PublicPointerDefs.h"

namespace sketch
{

// Forward-declare our resource type:
class Resource;

/**\brief A VTK geometry provider for our sketch resource.
  *
  * This class is responsible for providing a VTK data object
  * for each persistent object it is passed.
  * Not all objects need to have renderable geometry, but any
  * object that should be rendered must be handled here.
  */
class SKETCHSTEP4_SOLVED_EXPORT Geometry
  : public smtk::geometry::Cache<smtk::extension::vtk::geometry::Geometry>
{
public:
  // These configure the cache that comes with SMTK to hold VTK data objects
  // and expose type information so that VTK and ParaView adaptors can
  // find the proper class to handle any given persistent object.
  using CacheBaseType = smtk::extension::vtk::geometry::Geometry;
  smtkTypeMacro(smtk::extension::vtk::geometry::Geometry);
  smtkSuperclassMacro(smtk::geometry::Cache<CacheBaseType>);
  using DataType = Superclass::DataType;

  Geometry(const std::shared_ptr<sketch::Resource>& parent);
  ~Geometry() override = default;

  /// Return the resource this geometry provider targets.
  smtk::geometry::Resource::Ptr resource() const override;
  /// Given an object \a obj, populate its cache \a entry.
  void queryGeometry(const smtk::resource::PersistentObject::Ptr& obj, CacheEntry& entry)
    const override;
  /// Given an object \a obj, return the parametric dimension of its renderable geometry (or -1).
  int dimension(const smtk::resource::PersistentObject::Ptr& obj) const override;
  /// Provide information on how the renderable geometry should be mapped (as a surface, as glyphs, etc.).
  Purpose purpose(const smtk::resource::PersistentObject::Ptr& obj) const override;
  /// Ensure every renderable object has an up-to-date cache entry.
  /// This is a no-op for most resources.
  void update() const override;
  /// A method to fetch the bounds of renderable geometry.
  /// This is used for zooming to data.
  void geometricBounds(const DataType&, BoundingBox& bbox) const override;

protected:
  std::weak_ptr<sketch::Resource> m_parent;
};
} // namespace sketch

#endif
