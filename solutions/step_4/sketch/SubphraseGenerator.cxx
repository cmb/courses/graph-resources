// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "sketch/SubphraseGenerator.h"

#include "sketch/NodeGroupPhraseContent.h"
#include "sketch/Traits.h"

#include "smtk/view/ComponentPhraseContent.h"
#include "smtk/view/PhraseModel.h"
#include "smtk/view/ResourcePhraseContent.h"

#include "smtk/common/StringUtil.h"

#include <iostream>

using smtk::view::ComponentPhraseContent;
using smtk::view::ResourcePhraseContent;

static int phraseMutability =
  static_cast<int>(smtk::view::PhraseContent::ContentType::TITLE) |
  static_cast<int>(smtk::view::PhraseContent::ContentType::COLOR);

namespace sketch
{

SubphraseGenerator::SubphraseGenerator()
{
}

smtk::view::DescriptivePhrases SubphraseGenerator::subphrases(smtk::view::DescriptivePhrase::Ptr parent)
{
  smtk::view::DescriptivePhrases result;
  if (!parent)
  {
    return result;
  }
  auto* content = parent->content().get();
  const int mutability = static_cast<int>(smtk::view::PhraseContent::TITLE | smtk::view::PhraseContent::VISIBILITY);
  if (auto* componentContent = dynamic_cast<ComponentPhraseContent*>(content))
  {
    if (auto* group = dynamic_cast<sketch::Group*>(componentContent->relatedRawComponent()))
    {
      // Groups have members underneath them
      group->outgoing<GroupsToPaths>().visit([&result, &parent](const sketch::Path* constPath)
        {
          auto* path = const_cast<sketch::Path*>(constPath);
          result.push_back(
            ComponentPhraseContent::createPhrase(path->shared_from_this(), mutability, parent));
        }
      );
    }
    else if (auto* style = dynamic_cast<sketch::Style*>(componentContent->relatedRawComponent()))
    {
      // Styles have shapes underneath them
      style->outgoing<StylesToShapes>().visit([&result, &parent](const sketch::Shape* constShape)
        {
          auto* shape = const_cast<sketch::Shape*>(constShape);
          result.push_back(
            ComponentPhraseContent::createPhrase(shape->shared_from_this(), mutability, parent));
        }
      );
    }
    else if (auto* path = dynamic_cast<sketch::Path*>(componentContent->relatedRawComponent()))
    {
      // Paths have cusps underneath them
      path->outgoing<PathsToCusps>().visit([&result, &parent](const sketch::Cusp* constCusp)
        {
          auto* cusp = const_cast<sketch::Cusp*>(constCusp);
          result.push_back(
            ComponentPhraseContent::createPhrase(cusp->shared_from_this(), mutability, parent));
        }
      );
    }
    else if (auto* cusp = dynamic_cast<sketch::Cusp*>(componentContent->relatedRawComponent()))
    {
      // Cusps have no children.
    }
  }
  else if (auto* resourceContent = dynamic_cast<ResourcePhraseContent*>(content))
  {
    // Top-level resources have node groups underneath them
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Group")); // , "group"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Path")); // , "path"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Cusp")); // , "cusp"));
    result.push_back(sketch::NodeGroupPhraseContent::createPhrase(parent, "sketch::Style")); // , "style"));
  }
  else if (auto* nodeGroupContent = dynamic_cast<sketch::NodeGroupPhraseContent*>(content))
  {
    // Node groups hold "free" nodes of a given type underneath them.
    // If type is Style, then all Style nodes appear.
    // If type is Path, then Paths not owned by a Group appear.
    // If type is Group, then Groups not owned by a Group appear.
    // If type is Cusp, then all cusps appear (though no Cusp should ever be "free").
    std::shared_ptr<smtk::view::DescriptivePhrase> ancestor = parent->parent();
    std::shared_ptr<smtk::resource::Resource> resource = ancestor ? ancestor->relatedResource() : nullptr;
    while (ancestor && !resource)
    {
      ancestor = ancestor->parent();
      resource = ancestor ? ancestor->relatedResource() : nullptr;
    }
    if (resource)
    {
      auto childObjects = resource->filter(nodeGroupContent->childType());
      std::cout << "Filter <" << nodeGroupContent->childType() << "> produced " << childObjects.size() << "\n";
      for (const auto& child : childObjects)
      {
        result.push_back(ComponentPhraseContent::createPhrase(child, mutability, parent));
      }
      std::sort(result.begin(), result.end(), smtk::view::DescriptivePhrase::compareByTitle);
    }
  }
  return result;
}

bool SubphraseGenerator::hasChildren(const smtk::view::DescriptivePhrase& parent) const
{
  bool result = false;
  auto* content = parent.content().get();
  if (auto* componentContent = dynamic_cast<ComponentPhraseContent*>(content))
  {
    if (auto* group = dynamic_cast<sketch::Group*>(componentContent->relatedRawComponent()))
    {
      // Groups have members underneath them
      group->outgoing<GroupsToPaths>().visit([&result](const sketch::Path* path)
        {
          if (path)
          {
            result = true;
            return smtk::common::Visit::Halt;
          }
          return smtk::common::Visit::Continue;
        }
      );
    }
    else if (auto* style = dynamic_cast<sketch::Style*>(componentContent->relatedRawComponent()))
    {
      // Styles have shapes underneath them
      style->outgoing<StylesToShapes>().visit([&result](const sketch::Shape* shape)
        {
          if (shape)
          {
            result = true;
            return smtk::common::Visit::Halt;
          }
          return smtk::common::Visit::Continue;
        }
      );
    }
    else if (auto* path = dynamic_cast<sketch::Path*>(componentContent->relatedRawComponent()))
    {
      // Paths have cusps underneath them
      path->outgoing<PathsToCusps>().visit([&result](const sketch::Cusp* cusp)
        {
          if (cusp)
          {
            result = true;
            return smtk::common::Visit::Halt;
          }
          return smtk::common::Visit::Continue;
        }
      );
    }
    else if (auto* cusp = dynamic_cast<sketch::Cusp*>(componentContent->relatedRawComponent()))
    {
      // Cusps have no children.
      result = false;
    }
  }
  else if (auto* resourceContent = dynamic_cast<ResourcePhraseContent*>(content))
  {
    // Top-level resources have 4 node groups underneath them
    result = true;
  }
  else if (auto* nodeGroupContent = dynamic_cast<sketch::NodeGroupPhraseContent*>(content))
  {
    // Node groups hold "free" nodes of a given type underneath them.
    // If type is Style, then all Style nodes appear.
    // If type is Path, then Paths not owned by a Group appear.
    // If type is Group, then Groups not owned by a Group appear.
    // If type is Cusp, then all cusps appear (though no Cusp should ever be "free").
    auto resource = parent.relatedResource();
    auto childObjects = resource->filter(nodeGroupContent->childType());
    result = childObjects.empty();
  }
  return result;
}

smtk::resource::PersistentObjectSet SubphraseGenerator::parentObjects(
  const smtk::resource::PersistentObjectPtr& obj) const
{
  smtk::resource::PersistentObjectSet result;
  if (auto* shape = dynamic_cast<sketch::Shape*>(obj.get()))
  {
    if (auto* style = const_cast<sketch::Style*>(shape->style()))
    {
      // Shapes appear underneath 0 or 1 Styles.
      result.insert(style->shared_from_this());
    }
    if (auto* cusp = dynamic_cast<sketch::Cusp*>(shape))
    {
      // If the shape is a Cusp it appears underneath the Cusps NodeGroup and zero or more Paths.
      cusp->incoming<PathsToCusps>().visit(
        [&result](const sketch::Path* constParent)
        {
          auto* parent = const_cast<sketch::Path*>(constParent);
          result.insert(parent->shared_from_this());
        }
      );
    }
    else if (auto* path = dynamic_cast<sketch::Path*>(shape))
    {
      // If the shape is a Path it appears underneath the Paths NodeGroup and zero or more Groups.
      path->incoming<GroupsToPaths>().visit(
        [&result](const Group* constParent)
        {
          auto* parent = const_cast<sketch::Group*>(constParent);
          result.insert(parent->shared_from_this());
        }
      );
    }
  }
  else if (auto* group = dynamic_cast<sketch::Group*>(obj.get()))
  {
    // Groups appear underneath the Groups NodeGroup
    // and 0 or 1 Groups (if hierarchical groups are allowed; they aren't currently).
  }
  else if (auto* style = dynamic_cast<sketch::Style*>(obj.get()))
  {
    // Styles appear underneath the Styles NodeGroup and that's it.
  }
  return result;
}

void SubphraseGenerator::subphrasesForCreatedObjects(
    const smtk::resource::PersistentObjectArray& objects,
    const smtk::view::DescriptivePhrasePtr& localRoot,
    PhrasesByPath& resultingPhrases)
{
  // Add phrases underneath the fixed NodeGroupPhraseContent instances.
  // These nodes are under each resource, so we build a map of their locations first.
  struct Entry
  {
    SubphraseGenerator::Path path;
    std::shared_ptr<smtk::view::DescriptivePhrase> phrase;
  };
  std::map<smtk::resource::Resource*, std::map<std::string, Entry>> nodeTypes;

  // Scan for NodeGroupPhraseContent entries and add them to the map.
  int ii = -1;
  for (const auto& resourcePhrase : localRoot->subphrases())
  {
    ++ii;
    auto* resource = resourcePhrase->relatedResource().get();
    if (!resource) { continue; }
    std::map<std::string, Entry> blank;
    nodeTypes[resource] = blank;
    int jj = -1;
    for (const auto& subphrase : resourcePhrase->subphrases())
    {
      ++jj;
      if (auto* nodeGroup = dynamic_cast<sketch::NodeGroupPhraseContent*>(subphrase->content().get()))
      {
        nodeTypes[resource][nodeGroup->childType()] = Entry{{ii, jj}, subphrase};
      }
    }
  }

  // Now we can look up where to add each object.
  auto phrasesForObject(localRoot->phraseModel()->uuidPhraseMap()); // TODO: Fix SMTK to return const ref.
  std::vector<int> objectPath;
  for (const auto& object : objects)
  {
    auto node = std::dynamic_pointer_cast<sketch::Node>(object);
    if (!node)
    {
      continue;
    }
    auto* resource = node->parentResource();
    if (!resource)
    {
      continue;
    }
    auto resourceIt = nodeTypes.find(resource);
    if (resourceIt == nodeTypes.end())
    {
      continue;
    }
    std::string quotedTypeName = "'" + node->typeName() + "'";
    auto nodeTypeIt = resourceIt->second.find(quotedTypeName);
    if (nodeTypeIt == resourceIt->second.end())
    {
      continue;
    }
    objectPath = this->indexOfObjectInParent(object, nodeTypeIt->second.phrase, nodeTypeIt->second.path);
    resultingPhrases.insert(
      std::make_pair(objectPath, ComponentPhraseContent::createPhrase(
          node, phraseMutability, nodeTypeIt->second.phrase)));

    // Now, in addition to the object's entry under its proper NodeGroupPhraseContent,
    // we need to consider when an object must also appear as the child of another
    // object. Examples: cusps are children of the path they bound; shapes are children
    // of any group and style objects they are members of.
    if (auto shape = std::dynamic_pointer_cast<sketch::Shape>(node))
    {
      if (auto cusp = std::dynamic_pointer_cast<sketch::Cusp>(shape))
      {
        // Add cusp as a child to any relevant paths.
        cusp->incoming<PathsToCusps>().visit(
          [&](const sketch::Path* parent)
          {
            for (const auto& weakPhrase : phrasesForObject[parent->id()])
            {
              if (auto parentPhrase = weakPhrase.lock())
              {
                objectPath = this->indexOfObjectInParent(
                  cusp, parentPhrase, parentPhrase->index());
                if (!objectPath.empty())
                {
                  resultingPhrases.insert(
                    std::make_pair(objectPath, ComponentPhraseContent::createPhrase(
                        cusp, phraseMutability, parentPhrase)));
                }
              }
            }
          }
        );
      }
      // TODO: Add shape as a child to any relevant groups/styles.
      //       This should follow the pattern used for cusps above.
    }
  }
}

SubphraseGenerator::PhrasePath SubphraseGenerator::indexOfObjectInParent(
  const smtk::resource::PersistentObjectPtr& obj,
  const smtk::view::DescriptivePhrasePtr& parent,
  const PhrasePath& parentPath)
{
  const auto& subphrases = parent->subphrases();
  // TODO: We could bisect here (since phrases are assumed ordered).
  //       That would be much faster than this O(N) algorithm.

  SubphraseGenerator::PhrasePath result = parentPath;
  std::string objName = obj->name();
  int ii = 0;
  for (const auto& phrase : subphrases)
  {
    if (smtk::common::StringUtil::mixedAlphanumericComparator(objName, phrase->title()))
    {
      result.push_back(ii);
      return result;
    }
    ++ii;
  }
  result.push_back(static_cast<int>(subphrases.size()));
  return result;
}

} // namespace sketch
