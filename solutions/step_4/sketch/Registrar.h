//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_Registrar_h
#define sketch_Registrar_h

#include "sketch/Exports.h"

#include "smtk/attribute/Registrar.h"
#include "smtk/geometry/Manager.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Registrar.h"
#include "smtk/resource/Manager.h"
#include "smtk/view/Manager.h"

namespace sketch
{

class SKETCHSTEP4_SOLVED_EXPORT Registrar
{
public:
  /// List the registrars we depend on here (if needed).
  /// If a type-alias is named Dependencies and is a std::tuple, then the
  /// plugin manager will recursively invoke methods on each tuple entry
  /// before invoking methods this Registrar provides.
  using Dependencies = std::tuple<smtk::operation::Registrar, smtk::attribute::Registrar>;

  static void registerTo(const smtk::operation::Manager::Ptr&);
  static void unregisterFrom(const smtk::operation::Manager::Ptr&);

  static void registerTo(const smtk::resource::Manager::Ptr&);
  static void unregisterFrom(const smtk::resource::Manager::Ptr&);

  static void registerTo(const smtk::geometry::Manager::Ptr&);
  static void unregisterFrom(const smtk::geometry::Manager::Ptr&);

  static void registerTo(const smtk::view::Manager::Ptr&);
  static void unregisterFrom(const smtk::view::Manager::Ptr&);
};

} // namespace sketch

#endif
