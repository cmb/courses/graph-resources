//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_Create_h
#define sketch_Create_h

#include "sketch/Resource.h"

#include "smtk/operation/XMLOperation.h"

namespace sketch
{

/**\brief Create a sketch resource.
  */
class SKETCHSTEP4_SOLVED_EXPORT Create : public smtk::operation::XMLOperation
{
public:
  smtkTypeMacro(sketch::Create);
  smtkCreateMacro(Create);
  smtkSharedFromThisMacro(smtk::operation::Operation);
  smtkSuperclassMacro(smtk::operation::XMLOperation);

protected:
  Result operateInternal() override;
  const char* xmlDescription() const override;
  void markModifiedResources(Result&) override;
};

SKETCHSTEP4_SOLVED_EXPORT smtk::resource::ResourcePtr create(
  const smtk::common::UUID& uid,
  const std::shared_ptr<smtk::common::Managers>& managers = nullptr);

} // namespace sketch

#endif // sketch_Create_h
