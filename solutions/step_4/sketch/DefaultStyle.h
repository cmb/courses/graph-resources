// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step4_DefaultStyle_h
#define sketch_step4_DefaultStyle_h

#include "sketch/Exports.h"

namespace sketch
{

/**\brief An implicit arc that connects any Shape without
  *       a style to the resource's default style.
  *
  */
struct SKETCHSTEP4_SOLVED_EXPORT DefaultStyle
{
  using FromType = Shape;
  using ToType = Style;
  using Directed = std::true_type;
  static const std::size_t MaxOutDegree = 1; // Shapes can have at most 1 default style.

  /// Visit outgoing arcs from \a shape to 0 or 1 default style nodes.
  ///
  /// Declaring this method makes the arc implicit rather than explicit.
  template<typename Functor>
  smtk::common::Visited outVisitor(const Shape* shape, Functor ff) const;
};

} // namespace sketch

#endif // sketch_step4_DefaultStyle_h
