// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#include "Shape.h"
#include "DefaultStyle.h"
#include "DefaultStyle.txx"

namespace sketch
{

const Style* Shape::style() const
{
  const Style* result = this->incoming<StylesToShapes>().node();
  if (!result)
  {
    result = this->outgoing<DefaultStyle>().node();
  }
  return result;
}

} // namespace sketch
