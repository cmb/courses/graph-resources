//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef sketch_ApplicationConfiguration_h
#define sketch_ApplicationConfiguration_h

#include "sketch/Exports.h"

#include "smtk/extension/paraview/appcomponents/ApplicationConfiguration.h"

/**\brief Configure modelbuilder's resource-browser panel.
  */
class SKETCHSTEP4_SOLVED_EXPORT ApplicationConfiguration
  : public QObject
  , public smtk::paraview::ApplicationConfiguration

{
  Q_OBJECT
  Q_INTERFACES(smtk::paraview::ApplicationConfiguration)

public:
  ApplicationConfiguration(QObject* parent);
  ~ApplicationConfiguration() override = default;

  /**\brief Provide configuration information for panels.
    *
    */
  smtk::view::Information panelConfiguration(const QWidget* panel) override;
};

#endif // sketch_ApplicationConfiguration_h
