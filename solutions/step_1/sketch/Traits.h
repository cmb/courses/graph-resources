// Copyright © Kitware Inc under the [BSD-3-clause license](https://opensource.org/licenses/BSD-3-Clause).
#ifndef sketch_step1_Traits_h
#define sketch_step1_Traits_h

#include "sketch/Exports.h"

#include "smtk/graph/Component.h"

#include <type_traits> // for std::true_type
#include <tuple>
#include <vector>

namespace sketch
{

// Forward-declare node types

class Cusp;
class Group;
class Path;
class Shape;
class Style;

// Declare arc traits

/// Paths can be grouped together.
struct SKETCHSTEP1_SOLVED_EXPORT GroupsToPaths
{
  using FromType = Group;
  using ToType = Path;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 group.
};

/// Paths can be smooth, closed curves (no cusps) or have cusps (at ends or along interior).
struct SKETCHSTEP1_SOLVED_EXPORT PathsToCusps
{
  using FromType = Path;
  using ToType = Cusp;
  using Directed = std::true_type;
};

/// Paths can have a style.
struct SKETCHSTEP1_SOLVED_EXPORT StylesToShapes
{
  using FromType = Style;
  using ToType = Shape;
  using Directed = std::true_type;
  static const std::size_t MaxInDegree = 1; // Paths can have at most 1 style.
};

/// Node and arc types for hand drawings.
struct SKETCHSTEP1_SOLVED_EXPORT Traits
{
  // Note we do not need to list Node and Shape types in NodeTypes; they are not allowed
  // to exist in the graph... only their subclasses are.
  using NodeTypes = std::tuple<Cusp, Group, Path, Style>;
  using ArcTypes = std::tuple<GroupsToPaths, PathsToCusps, StylesToShapes>;
};

class SKETCHSTEP1_SOLVED_EXPORT Node : public smtk::graph::Component
{
public:
  smtkTypeMacro(sketch::Node);
  smtkSuperclassMacro(smtk::graph::Component);

  template<typename... Args>
  Node(Args&&... args)
    : smtk::graph::Component(std::forward<Args>(args)...)
  {
  }

  std::string name() const override
  {
    if (this->properties().contains<std::string>("name"))
    {
      return this->properties().at<std::string>("name");
    }
    return this->Superclass::name();
  }
  void setName(const std::string& nodeName)
  {
    this->properties().get<std::string>()["name"] = nodeName;
  }
};

// This is a new class introduced for EXERCISE 3 as a common base for Path and Cusp.
class SKETCHSTEP1_SOLVED_EXPORT Shape : public Node
{
public:
  smtkTypeMacro(sketch::Shape);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Shape(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a C-0 point on a path (i.e., a corner or endpoint).
class SKETCHSTEP1_SOLVED_EXPORT Cusp : public Shape
{
public:
  smtkTypeMacro(sketch::Cusp);
  smtkSuperclassMacro(sketch::Shape);

  template<typename... Args>
  Cusp(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  // EXERCISE 2:
  /// Set coordinates on a cusp node using SMTK's property system.
  void setCoordinates(const std::vector<double>& point)
  {
    auto& coords = this->properties().get<std::vector<double>>()["coordinates"];
    coords = point;
  }

  /// Fetch coordinates on cusp node using SMTK's property system.
  const std::vector<double>& coordinates() const
  {
    if (this->properties().contains<std::vector<double>>("coordinates"))
    {
      return this->properties().at<std::vector<double>>("coordinates");
    }
    // If the property is not present (because no coordinates have been set),
    // return a blank vector rather than crash trying to dereference a
    // non-existent entry.
    static std::vector<double> dummy;
    return dummy;
  }
};

/// A node representing a collection of paths.
class SKETCHSTEP1_SOLVED_EXPORT Group : public Node
{
public:
  smtkTypeMacro(sketch::Group);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Group(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing a single stroke of a pen.
class SKETCHSTEP1_SOLVED_EXPORT Path : public Shape
{
public:
  smtkTypeMacro(sketch::Path);
  smtkSuperclassMacro(sketch::Shape);

  template<typename... Args>
  Path(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }
};

/// A node representing the drawing style for a path (e.g. color, thickness, ...)
class SKETCHSTEP1_SOLVED_EXPORT Style : public Node
{
public:
  smtkTypeMacro(sketch::Style);
  smtkSuperclassMacro(sketch::Node);

  template<typename... Args>
  Style(Args&&... args)
    : Superclass(std::forward<Args>(args)...)
  {
  }

  /// Set/get a color to use when rendering lines.
  ///
  /// Note that this is stored as a member variable - not a property - while
  /// line thickness is stored as a property. This was done for illustrative
  /// purposes.
  void setLineColor(const std::array<double, 4>& color)
  {
    m_lineColor = color;
  }
  const std::array<double, 4>& lineColor() const { return m_lineColor; }

  /// Set/get a thickness to use for lines when rendering.
  void setLineThickness(double thickness)
  {
    this->properties().get<double>()["line_thickness"] = thickness;
  }
  double lineThickness() const
  {
    if (this->properties().contains<double>("line_thickness"))
    {
      return this->properties().at<double>("line_thickness");
    }
    return -1.0;
  }

protected:
  std::array<double, 4> m_lineColor{0.9, 0.9, 0.9, 1.0};
};

} // namespace sketch

#endif // sketch_step1_Traits_h
