Step 1 Solutions
----------------

0. You should see a graph connecting path1 and path2 to group; path1 to plain; and path2 to fancy.
   This solutions directory has an image named ``exercise0.png`` that should match yours.

   .. figure:: exercise0.png

      This is what the result should look like.

1. Look in the ``Exercise1.cxx`` file in this directory for the solution.
   This will be built by default, so you can inspect the output by running the test:

   .. code-block:: bash

      ./bin/solutions_step_1 Solution1_1 > graph.dot
      dot -Tpng graph.dot > graph.png
      eog graph.png

   .. figure:: exercise1.png

      This is what the result should look like.

2. This involves modifying the ``sketch/Traits.h`` file; the version in this directory is
   different than the one in the directory above.
   Use diff (or vimdiff) to compare them, but be aware that ``sketch/Traits.h`` in this
   directory also contains the answer to exercise 3 below.

3. Two alternatives are:

   a. Add a new ``CuspsToStyles`` arc type with ``FromType = Cusp`` and ``ToType = Style``.

   b. Make ``Cusp`` and ``Path`` derive from a new class (let's call it ``Shape``) and
      change the ``PathsToStyles`` arc to use the base ``Shape`` class as its ``FromType``.

   While the first option might seem easier at first, the more arc types you add, the
   more difficult it is to get all the nodes to which a ``Style`` applies – because you
   must iterate over all the possible arc types to fetch them.

   The latter option works in this case but note you only get to "spend" inheritance on one
   relationship; if you use inheritance to simplify the types of nodes that styles
   apply to, what happens if you want ``Group`` nodes to connect to ``Path`` nodes
   and other ``Group`` nodes but not ``Cusp`` nodes (i.e., you want to allow groups of groups)?

   Overall, using inheritance to relate node types that are specializations
   of the same concept is preferable for other reasons, so the ``Traits.h`` in the
   solutions directory uses the second approach. If you want to go farther and support
   groups of groups, then you'll need to introduce a new arc type for this (arguably distinct)
   type of relationship.

4. You should get a build error. Adding the following line

   .. code-block:: c++

      fancy->outgoing<StylesToPaths>().connect(cusp1);

   to ``Exercise1.cxx`` file in the **problem** directory (not this directory)
   will cause the following build error using GCC's c++ compiler.
   (The line above will compile in this directory because it includes
   a solution to exercise 3.)

   .. code-block:: text

      /usr/bin/c++ -DSCRATCH_DIR=\"/home/kitware/tutorial/graph-resources/build/Testing/Temporary\" -I/home/kitware/tutorial/graph-resources/build/step_1/problems -I/home/kitware/tutorial/graph-resources/src/step_1 -I/home/kitware/tutorial/graph-resources/build/step_1 -isystem /home/kitware/build/install/include -isystem /home/kitware/build/install/include/smtk/22.05.0 -g -std=gnu++11 -MD -MT step_1/problems/CMakeFiles/Exercises_step_1_problems.dir/Exercise1.cxx.o -MF step_1/problems/CMakeFiles/Exercises_step_1_problems.dir/Exercise1.cxx.o.d -o step_1/problems/CMakeFiles/Exercises_step_1_problems.dir/Exercise1.cxx.o -c /home/kitware/tutorial/graph-resources/src/step_1/problems/Exercise1.cxx
      /home/kitware/tutorial/graph-resources/src/step_1/problems/Exercise1.cxx: In function ‘int Exercise1(int, char**)’:
      /home/kitware/tutorial/graph-resources/src/step_1/problems/Exercise1.cxx:31:43: error: no matching function for call to ‘smtk::graph::ArcEndpointInterface<sketch::StylesToPaths, smtk::graph::ArcConstness<false>, smtk::graph::ArcDirection<true> >::connect(std::shared_ptr<sketch::Cusp>&)’
         31 |   fancy->outgoing<StylesToPaths>().connect(cusp1);
            |   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~~~
      In file included from /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/ArcMap.h:18,
                       from /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/Component.h:19,
                       from /home/kitware/tutorial/graph-resources/src/step_1/Traits.h:7,
                       from /home/kitware/tutorial/graph-resources/src/step_1/Resource.h:5,
                       from /home/kitware/tutorial/graph-resources/src/step_1/problems/Exercise1.cxx:1:
      /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/ArcImplementation.h:732:8: note: candidate: ‘bool smtk::graph::ArcEndpointInterface<TraitsType, Const, Outgoing>::connect(const OtherType*) [with TraitsType = sketch::StylesToPaths; Const = smtk::graph::ArcConstness<false>; Outgoing = smtk::graph::ArcDirection<true>; OtherType = sketch::Path]’
        732 |   bool connect(const OtherType* other)
            |        ^~~~~~~
      /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/ArcImplementation.h:732:33: note:   no known conversion for argument 1 from ‘std::shared_ptr<sketch::Cusp>’ to ‘const smtk::graph::ArcEndpointInterface<sketch::StylesToPaths, smtk::graph::ArcConstness<false>, smtk::graph::ArcDirection<true> >::OtherType*’ {aka ‘const sketch::Path*’}
        732 |   bool connect(const OtherType* other)
            |                ~~~~~~~~~~~~~~~~~^~~~~
      /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/ArcImplementation.h:739:8: note: candidate: ‘bool smtk::graph::ArcEndpointInterface<TraitsType, Const, Outgoing>::connect(const std::shared_ptr<typename std::conditional<Outgoing::value, typename smtk::graph::ArcImplementation<TraitsType>::ToType, typename smtk::graph::ArcImplementation<TraitsType>::FromType>::type>&) [with TraitsType = sketch::StylesToPaths; Const = smtk::graph::ArcConstness<false>; Outgoing = smtk::graph::ArcDirection<true>; typename std::conditional<Outgoing::value, typename smtk::graph::ArcImplementation<TraitsType>::ToType, typename smtk::graph::ArcImplementation<TraitsType>::FromType>::type = sketch::Path; typename smtk::graph::ArcImplementation<TraitsType>::FromType = sketch::Style; typename smtk::graph::ArcImplementation<TraitsType>::ToType = sketch::Path]’
        739 |   bool connect(const std::shared_ptr<OtherType>& other) { return this->connect(other.get()); }
            |        ^~~~~~~
      /home/kitware/build/install/include/smtk/22.05.0/smtk/graph/ArcImplementation.h:739:50: note:   no known conversion for argument 1 from ‘std::shared_ptr<sketch::Cusp>’ to ‘const std::shared_ptr<sketch::Path>&’
        739 |   bool connect(const std::shared_ptr<OtherType>& other) { return this->connect(other.get()); }
            |                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^~~~~
